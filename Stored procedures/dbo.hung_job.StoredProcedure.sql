USE [meta]
GO
/****** Object:  StoredProcedure [dbo].[hung_job]    Script Date: 20.07.2017 12:46:00 *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[hung_job]
	  @jobName nvarchar(128)
	 ,@IsForced bit =1						-- принудительная остановка джоба, несмотря ни на что
	 ,@max_job_duration_minutes int =null	-- максимально возможная продолжительность джоба
AS

declare @job_duration_minutes int, @result bit

if (@jobName is null or not exists (select 1 from msdb.dbo.sysjobs where name = @JobName))
begin
	raiserror('Неправильно указано имя джоба!',16,1)
	return
end

select
		 @job_duration_minutes = datediff(minute,sja.start_execution_date,getdate())	-- сколько минут джоб выполняется		
	from msdb.dbo.sysjobactivity sja
		inner join msdb.dbo.sysjobs sj on sja.job_id = sj.job_id
		inner join msdb.dbo.syssessions ss on sja.session_id = ss.session_id
	where sj.name = @jobName
		and sja.start_execution_date is not null and sja.stop_execution_date is null		-- определяем только выполняющиеся джобы
		and ss.agent_start_date = (select max(agent_start_date) session_date from [msdb].[dbo].[syssessions])

if (@@rowcount = 0)				-- если нужный джоб не выполняется - ничего не делаем
	return

if (@IsForced = 1 or @max_job_duration_minutes is null or @job_duration_minutes >= @max_job_duration_minutes)
begin
	exec @result = msdb.dbo.sp_stop_job @job_name = @JobName;		-- принудительная остановка джоба
	
	if (@result = 1)
		raiserror('Ошибка принудительной остановки джоба!',16,1)
end

return

GO
