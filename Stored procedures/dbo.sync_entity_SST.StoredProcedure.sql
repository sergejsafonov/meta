USE [meta]
GO

alter PROCEDURE [dbo].[sync_entity_SST]
AS

update e
	set [object_id] = so.[object_id]
	from dbo.entity_SST e
		inner join [STG_SSD].[sys].[schemas] sc on e.schemaName = sc.name
		inner join [STG_SSD].[sys].[objects] so on e.entityName = so.name and sc.[schema_id] = so.[schema_id]
	where e.[object_id] <> so.[object_id] or e.[object_id] is null

update e
	set entityName = so.name
	from dbo.entity_SST e
		inner join [STG_SSD].[sys].[objects] so on e.[object_id] = so.[object_id]
		inner join [STG_SSD].[sys].[schemas] sc on so.schema_id = sc.schema_id and sc.[name] = e.schemaName
	where e.entityName <> '[' + so.name + ']'

delete from e
	from dbo.entity_SST e
	where not exists (select 1 from [stg_SSD].[sys].[objects] so 
						inner join [STG_SSD].[sys].[schemas] sc on so.schema_id = sc.schema_id where e.[object_id] = so.[object_id] or e.[object_id] is null)	--e.entityName = '[' + so.name + ']' and e.schemaName = sc.name)			

insert into dbo.entity_SST (schemaName, entityName, [object_id])
	SELECT sch.[name] as schemaName
		,'[' + so.name + ']' as entityName
		,so.[object_id]
    FROM [STG_SSD].[sys].[objects] so
		inner join [STG_SSD].[sys].[schemas] sch on so.[schema_id] = sch.[schema_id]
	where so.type = 'U' 
		and (sch.[name] <> 'xlsi'
			or so.Name in ('IncomeFromMagazine','UnaccountedMaterial','LombardLom'))
		and not exists (select 1 from meta.dbo.entity_SST e where so.[object_id] = e.[object_id])
	order by entityName

update dbo.entity_SST
	set isXLSI = 1
	where schemaName = 'xlsi'

update dbo.fileTransfer
	set isIncremental = 0
-- select * from dbo.fileTransfer
	where isIncremental = 1 and isNightFull = 1
update dbo.fileTransfer
	set isIncremental = 1
-- select * from dbo.fileTransfer
	where isIncremental = 0 and isNightFull = 0

return

GO

-- EXEC [dbo].[sync_entity_SST]