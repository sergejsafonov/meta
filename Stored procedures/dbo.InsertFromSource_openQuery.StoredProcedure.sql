USE [meta]
GO
/****** Object:  StoredProcedure [dbo].[InsertFromSource_openQuery]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE [dbo].[InsertFromSource_openQuery]
	@schemaName NVARCHAR(128) = NULL,
	@entityName NVARCHAR(128) = NULL,
	@typeLoad NVARCHAR(32) = 'incremental'
AS

set nocount on

declare @rowCountUp int, @rowStepUp int, @rowCount int, @rowStep int, @selectStr nvarchar(max), @entityID int, @queryStr nvarchar(max)
declare @attributeName nvarchar(128), @insertStr nvarchar(max), @isFullDelete bit, @isPK bit, @isAddPK BIT--, @isSync BIT

-- определим количество иттераций по создаваемым таблицам для цикла
if (@entityName is not null)
	select @rowCountUp = 1
ELSE
	SELECT @rowCountUp = COUNT(*) 
		FROM dbo.entity_STG e
		WHERE e.isUse = 1 AND (e.isPOS = 1 OR e.isPOSLoy = 1 OR e.isPOSSet = 1 OR e.isRstat = 1 OR e.isFact = 0)

set @rowStepUp = 1
WHILE (@rowStepUp <= @rowCountUp)
BEGIN
	if (@rowStepUp = 1 and @entityName is not null and @schemaName is not null)
		select 
				 @entityID = e.entityID,
				 @isFullDelete = e.isFullDelete
			from dbo.entity_STG e
			where e.schemaName = @schemaName and e.entityName = @entityName and e.isUse = 1 and (e.isPOS = 1 or e.isPOSLoy = 1 or e.isPOSSet = 1 or e.isRstat = 1 or e.isFact = 0)
	ELSE
		SELECT
			 @entityID = tt.entityID 
			,@entityName = tt.entityName
			,@schemaName = tt.schemaName
			,@isFullDelete = tt.isFullDelete
		FROM (SELECT ROW_NUMBER() OVER (ORDER BY e.sortOrder, e.entityID) AS rowNum
					,e.entityID
					,e.entityName
					,e.schemaName
					,e.isFullDelete
				FROM dbo.entity_STG e
				WHERE e.isUse = 1 AND (e.isPOS = 1 OR e.isPOSLoy = 1 OR e.isPOSSet = 1 OR e.isRstat = 1 OR e.isFact = 0)
			) tt WHERE tt.rowNum = @rowStepUp
	
	set @insertStr = 'insert into stg.' + @schemaName + '.' + @entityName + ' ('
	set @queryStr = ' select '

	set @selectStr = ''
	exec dbo.get_selectFromSource @schemaName, @entityName, @typeLoad, @selectStr output	
	
	select @rowCount = count(*) 
		from dbo.attribute_STG a
			inner join dbo.entity_STG e on a.entityID = e.entityID
		where a.isUse = 1 and e.schemaName = @schemaName and a.entityName = @entityName 

	set @rowStep = 1
	-- цикл определения признаков столбцов для создания таблицы
	WHILE (@rowStep <= @rowCount)
	BEGIN
		select 
				 @attributeName = tt.attributeName,
				 @isPK = tt.isPK,
				 @isAddPK = tt.isAddPK
			from (
				select row_number() over (order by a.columnID) AS rowNum,
					a.attributeName,
					a.isPK,
					a.isAddPK
				from dbo.attribute_STG a
				where a.isUse = 1 and a.entityID = @entityID
			) tt where tt.rowNum = @rowStep

		if (@isFullDelete = 0 or @isPK = 1 or @isAddPK = 1 or @typeLoad in ('incremental','everyHour'))
		begin
			-- определим строку для каждого столбца
			set @insertStr = @insertStr + '['+@attributeName+']'
			set @queryStr = @queryStr + '['+@attributeName+']'

			IF (@rowStep < @rowCount)
			BEGIN
				set @insertStr = @insertStr + ','
				SET @queryStr = @queryStr + ','
			END
		END
			
		set @rowStep = @rowStep + 1	
	END

	IF (RIGHT(@insertStr,1) <> '(')
	BEGIN
		IF (RIGHT(@insertStr,1) = ',')
			SET @insertStr = LEFT(@insertStr,LEN(@insertStr)-1)
		IF (RIGHT(@queryStr,1) = ',')
			SET @queryStr = LEFT(@queryStr,LEN(@queryStr)-1)

		SET @insertStr = @insertStr + ')'

		IF (@schemaName IN ('axp'))
			SET @queryStr = ' ' + @selectStr
		ELSE
			SET @queryStr = @queryStr + ' from openquery(' + @schemaName + ', ''' + @selectStr + ''')'

		SET @queryStr = @insertStr + @queryStr

		EXEC dbo.truncate_Stg_tables @schemaName, @entityName--, @isSync=0;

--		select @queryStr
		PRINT @queryStr
		EXEC (@queryStr)
	END
			
	SET @rowStepUp = @rowStepUp + 1	
END

/*
Exec dbo.InsertFromSource_openQuery 'sap','mard'
Exec dbo.InsertFromSource_openQuery 'sap','mara'
Exec dbo.InsertFromSource_openQuery 'sap','ZTB_STATUS_ORDER'
Exec dbo.InsertFromSource_openQuery 'sap','tcla'
Exec dbo.InsertFromSource_openQuery 'sap','inob'
Exec dbo.InsertFromSource_openQuery 'sap','cabn'
Exec dbo.InsertFromSource_openQuery 'sap','cabnt'
Exec dbo.InsertFromSource_openQuery 'sap','cawn'
Exec dbo.InsertFromSource_openQuery 'sap','cawnt'

Exec dbo.InsertFromSource_openQuery 'sap','vttp' --isfact=0
Exec dbo.InsertFromSource_openQuery 'posr','od_purchase','weekFull'
Exec dbo.InsertFromSource_openQuery 'posr','od_position','weekFull'
Exec dbo.InsertFromSource_openQuery 'posr','od_session','weekFull'
Exec dbo.InsertFromSource_openQuery 'axp','od_position'

select count(*),min(datecommit) from stg.posr.od_purchase				-- 725110	2017-01-29 05:17:42.0190000
select count(*) from stg.posr.od_position								-- 1788502
select count(*) from stg.posr.od_session								-- 319870
select * from stg.zbp.ZSTORE_CHNG

Select * from meta.dbo.DWH_UsedParam_by_Stored_Proc('Function')
Select * from meta.dbo.entity_STG where selectStr like '%function%'
*/
GO
