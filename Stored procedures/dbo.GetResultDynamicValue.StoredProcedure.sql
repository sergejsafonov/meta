USE [meta]
GO
/****** Object:  StoredProcedure [dbo].[GetResultDynamicValue]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetResultDynamicValue]
as

declare @rowCount int, @rowStep int, @Name nvarchar(32), @DynamicPathValue nvarchar(1024), @ResultDynamicPathValue nvarchar(64)
declare @DynamicFileValue nvarchar(1024), @ResultDynamicFileValue nvarchar(64)

select @rowCount = count(Name)
	from dbo.fileTransfer
	where (DynamicPathValue is not null or DynamicFileValue is not null)

set @rowStep = 1
while (@rowStep <= @rowCount)
begin
	select
		@Name = tt.Name,
		@DynamicPathValue = tt.DynamicPathValue,
		@DynamicFileValue = tt.DynamicFileValue
	from (select row_number() over (order by fileTransferID) AS rowNum
					,Name
					,DynamicPathValue
					,DynamicFileValue
				from dbo.fileTransfer
				where (DynamicPathValue is not null or DynamicFileValue is not null)
			) tt where tt.rowNum = @rowStep		

	if (@DynamicPathValue is not null)			
		set @DynamicPathValue = 'update dbo.fileTransfer set ResultDynamicPathValue = ' + @DynamicPathValue + ' where Name = ''' + @Name + ''''
	
	if (@DynamicFileValue is not null)
		set @DynamicFileValue = 'update dbo.fileTransfer set ResultDynamicFileValue = ' + @DynamicFileValue + ' where Name = ''' + @Name + ''''

	exec (@DynamicPathValue)
	exec (@DynamicFileValue)

	set @DynamicPathValue = null
	set @DynamicFileValue = null

	set @rowStep = @rowStep + 1	
end

-- exec meta.dbo.GetResultDynamicValue	
GO
