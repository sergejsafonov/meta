USE [meta]
GO
/****** Object:  StoredProcedure [dbo].[reset_flagOperation]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[reset_flagOperation]
	@isFromDM bit = 0,
	@schemaName nvarchar(128) = null,
	@entityName nvarchar(128) = null
AS

declare @rowCount int, @rowStep int, @attributeName nvarchar(128), @entityID int, @isSCD2 bit
declare @updateStr nvarchar(max), @deleteStr nvarchar(max), @isFact bit, @isResetFlag bit, @isDM bit

if (@schemaName is null and @entityName is not null or @schemaName not in ('sap','pos','dbo','xls','crm','rstat','axp','dim','bpm'))
begin
	raiserror('Не указана @schemaName!',16,1)
	return
end

begin try
	SET CONTEXT_INFO 0x0101010101;

	-- определим количество иттераций по создаваемым таблицам для цикла
	if (@entityName is not null)
		select @rowCount = 1
	else
		select @rowCount = count(*) 
			from dbo.entity_DWH e
				inner join dbo.attribute_DWH a on e.entityName = a.entityName and e.schemaName = a.schemaName
			where e.isUse = 1 and a.isUse = 1 and a.is_FlagOperation = 1
		
	set @rowStep = 1
	while (@rowStep <= @rowCount)
	begin
		if (@rowStep = 1 and @entityName is not null)
			select 
					 @entityID = e.entityID,
					 @schemaName = e.schemaName,
					 @isFact = e.isFact,
					 @isResetFlag = e.isResetFlag,
					 @isSCD2 = e.isSCD2,
					 @isDM = e.isDM,
					 @attributeName = a.attributeName
				from dbo.entity_DWH e
					inner join dbo.attribute_DWH a on e.entityID = a.entityID
				where e.isUse = 1 and a.isUse = 1 and a.is_FlagOperation = 1 and e.entityName = @entityName and e.schemaName = @schemaName
		else
			select
				 @entityID = tt.entityID 
				,@entityName = tt.entityName
				,@schemaName = tt.schemaName
				,@isFact = tt.isFact
				,@isResetFlag = tt.isResetFlag
				,@isSCD2 = tt.isSCD2
				,@isDM = tt.isDM
				,@attributeName = tt.attributeName
			from (select row_number() over (order by e.sortOrder desc, e.entityID) AS rowNum
						,e.entityID
						,e.entityName
						,e.schemaName
						,e.isFact
						,e.isResetFlag
						,e.isSCD2
						,e.isDM
						,a.attributeName
					from dbo.entity_DWH e
						inner join dbo.attribute_DWH a on e.entityID = a.entityID
					where e.isUse = 1 and a.isUse = 1 and a.is_FlagOperation = 1
				) tt where tt.rowNum = @rowStep
					
		if (@rowCount = 1 or not (@schemaName = 'sap' and @entityName in ('DealTransaction','TransactionActivity','Condition','MaterialPerPricelistCondition')))
		begin
			if (@isFact = 1 and @isSCD2 = 0 and @isFromDM = @isDM)
			begin
				set @deleteStr = 'delete from DWH.[' + @schemaName + '].[' + @entityName + '] where ' + @attributeName + ' = 3'
		--		set @updateStr = 'update DWH.' + @schemaName + '.' + @entityName + ' set ' + @attributeName + ' = 0 where ' +  @attributeName + ' = 2'
		--		для RemainsCurrent обязательно сбрасывать все признаки обновления, и 1 и 2!

		--		select @deleteStr
				exec (@deleteStr)
			end
			if (@isResetFlag = 1 and @isFromDM = @isDM)
				set @updateStr = 'update DWH.[' + @schemaName + '].[' + @entityName + '] set ' + @attributeName + ' = 0 where ' +  @attributeName + ' in (1,2,4,5)'
			else
				set @updateStr = null

--			select @updateStr
			exec (@updateStr)
		end

		set @rowStep = @rowStep + 1	
	end
end try

begin catch
	if (@@ERROR <> 4701)
		throw;

	select ERROR_MESSAGE()
end catch


GO
