USE [meta]
GO
/****** Object:  StoredProcedure [dbo].[create_index_flagOperation]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[create_index_flagOperation]
	@schemaName nvarchar(128) = null,
	@entityName nvarchar(128) = null
AS

declare @rowCount int, @rowStep int, @attributeName nvarchar(128), @entityID int, @index nvarchar(max), @isColumnStore bit, @object_id int

if (@schemaName is null and @entityName is not null or @schemaName not in ('sap','pos','dbo','xls','crm','rstat'))
begin
	raiserror('Не указана @schemaName!',16,1)
	return
end

-- определим количество иттераций по создаваемым таблицам для цикла
if (@entityName is not null)
	select @rowCount = 1
else
	select @rowCount = count(*) 
		from dbo.entity_DWH e
			inner join dbo.attribute_DWH a on e.entityName = a.entityName and e.schemaName = a.schemaName
		where e.isUse = 1 and a.isUse = 1 and a.is_FlagOperation = 1
		
set @rowStep = 1
while (@rowStep <= @rowCount)
begin
	if (@rowStep = 1 and @entityName is not null)
		select 
				 @entityID = e.entityID,
				 @schemaName = e.schemaName,
				 @attributeName = a.attributeName,
				 @isColumnStore = e.isColumnStore
			from dbo.entity_DWH e
				inner join dbo.attribute_DWH a on e.entityID = a.entityID
			where e.isUse = 1 and a.isUse = 1 and a.is_FlagOperation = 1 and e.entityName = @entityName and e.schemaName = @schemaName
	else
		select
			 @entityID = tt.entityID 
			,@entityName = tt.entityName
			,@schemaName = tt.schemaName
			,@attributeName = tt.attributeName
			,@isColumnStore = tt.isColumnStore
		from (select row_number() over (order by e.sortOrder, e.entityID) AS rowNum
					,e.entityID
					,e.entityName
					,e.schemaName
					,a.attributeName
					,e.isColumnStore
				from dbo.entity_DWH e
					inner join dbo.attribute_DWH a on e.entityID = a.entityID
				where e.isUse = 1 and a.isUse = 1 and a.is_FlagOperation = 1 --and e.isColumnStore = 0
			) tt where tt.rowNum = @rowStep
	
	select @object_id = [object_id] from [dwh].[sys].[indexes] where name = 'i_' + @entityName + '_' + @attributeName
	
	if @object_id is null
		set @index = 'create nonclustered index i_' + @entityName + '_' +  + @attributeName + ' on DWH.' + @schemaName + '.' + @entityName + ' (' + @attributeName + ')'
	else
		set @index = null

	if (@index is not null)
	begin		
--		select @index 
		exec (@index)
	end

	set @rowStep = @rowStep + 1	
end


GO
