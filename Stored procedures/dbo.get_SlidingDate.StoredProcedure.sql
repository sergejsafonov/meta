USE [meta]
GO
/****** Object:  StoredProcedure [dbo].[get_SlidingDate]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[get_SlidingDate]
	@schemaName nvarchar(128) = null,
	@entityName nvarchar(128) = null,
	@typeLoad nvarchar(32) = 'incremental',
	@SlidingDateVar nchar(8) = null output
AS

declare @SlidingValue smallint, @SlidingValueNight smallint, @SlidingValueWeek smallint, @SlidingDate date
	
select @SlidingValue = SlidingValue,
		@SlidingValueNight = SlidingValueNight,
		@SlidingValueWeek = SlidingValueWeek
	from dbo.SlidingDate 
	where EntityName = @EntityName and SchemaName = @SchemaName
if (@typeLoad in ('incremental','everyHour'))	
	set @SlidingDate = dateadd(day, -@SlidingValue, cast(getdate() as date))
else if (@typeLoad = 'nightFull')
	set @SlidingDate = dateadd(day, -@SlidingValueNight, cast(getdate() as date))
else if (@typeLoad = 'weekFull')
	set @SlidingDate = dateadd(day, -@SlidingValueWeek, cast(getdate() as date))
set @SlidingDateVar = convert(nchar(8), @SlidingDate, 112)
Print 'Value: '+ isnull(try_convert(nvarchar(4),@SlidingValue),'0') + ' -> '+ isnull(try_convert(nvarchar,@SlidingDate),'0') + ' -> ' + isnull(try_convert(nvarchar,@SlidingDateVar),'0')
return 

GO
