USE [meta]
GO
/****** Object:  StoredProcedure [dbo].[InsertPOS_openQuery]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[InsertPOS_openQuery]
	@schemaName nvarchar(128) = null,
	@entityName nvarchar(128) = null,
	@isIncremental bit =1
AS
set nocount on
declare @rowCountUp int, @rowStepUp int, @rowCount int, @rowStep int, @selectStr nvarchar(max), @entityID int, @queryStr nvarchar(max)
declare @attributeName nvarchar(128), @insertStr nvarchar(max)

-- определим количество иттераций по создаваемым таблицам для цикла
if (@entityName is not null)
	select @rowCountUp = 1
else
	select @rowCountUp = count(*) 
		from dbo.entity_STG e
		where e.isUse = 1 and e.isPOS = 1

set @rowStepUp = 1
while (@rowStepUp <= @rowCountUp)
begin
	if (@rowStepUp = 1 and @entityName is not null and @schemaName is not null)
		select 
				 @entityID = e.entityID
			from dbo.entity_STG e
			where e.schemaName = @schemaName and e.entityName = @entityName and e.isUse = 1 and e.isPOS = 1
	else
		select
			 @entityID = tt.entityID 
			,@entityName = tt.entityName
			,@schemaName = tt.schemaName
		from (select row_number() over (order by e.sortOrder desc, e.entityID) AS rowNum
					,e.entityID
					,e.entityName
					,e.schemaName
				from dbo.entity_STG e
				where e.isUse = 1 and e.isPOS = 1 
			) tt where tt.rowNum = @rowStepUp
	
	set @insertStr = 'insert into stg.' + @schemaName + '.' + @entityName + ' ('
	set @queryStr = ' select '

	set @selectStr = ''
	exec dbo.get_selectFromSource @schemaName, @entityName, @isIncremental, @selectStr output	
--	select @selectStr
	
	select @rowCount = count(*) 
		from dbo.attribute_STG a
			inner join dbo.entity_STG e on a.entityID = e.entityID
		where a.isUse = 1 and e.schemaName = @schemaName and a.entityName = @entityName 

	set @rowStep = 1
	-- цикл определения признаков столбцов для создания таблицы
	while (@rowStep <= @rowCount)
	begin
		select 
				 @attributeName = tt.attributeName
			from (
				select row_number() over (order by a.columnID) AS rowNum,
					a.attributeName
				from dbo.attribute_STG a
				where a.isUse = 1 and a.entityID = @entityID
			) tt where tt.rowNum = @rowStep

		-- определим строку для каждого столбца
		set @insertStr = @insertStr + @attributeName
		set @queryStr = @queryStr + @attributeName

		if (@rowStep < @rowCount)
		begin
			set @insertStr = @insertStr + ','
			set @queryStr = @queryStr + ','
		end

		set @rowStep = @rowStep + 1	
	end

	set @insertStr = @insertStr + ')'
	set @queryStr = @queryStr + ' from openquery(' + @schemaName + ', ''' + @selectStr + ''')'
	
	set @queryStr = @insertStr + @queryStr

	exec dbo.truncate_Stg_tables @schemaName, @entityName

--	select @queryStr
	exec (@queryStr)
			
	set @rowStepUp = @rowStepUp + 1	
end


GO
