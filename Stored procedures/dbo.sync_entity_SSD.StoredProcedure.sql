USE [meta]
GO

ALTER PROCEDURE [dbo].[sync_entity_SSD]
AS

update e
	set [object_id] = so.[object_id]
	from dbo.entity_SSD e
		inner join [SSD].[sys].[objects] so on e.entityName = so.name
		inner join [SSD].[sys].[schemas] sc on so.schema_id = sc.schema_id and sc.[name] = e.schemaName
	where (e.[object_id] <> so.[object_id] or e.[object_id] is null) 

update e
	set entityName = so.name
	from dbo.entity_SSD e
		inner join SSD.[sys].[objects] so on e.[object_id] = so.[object_id]
		inner join SSD.[sys].[schemas] sc on so.schema_id = sc.schema_id and sc.[name] = e.schemaName
	where e.entityName <> so.name 

delete from e
	from dbo.entity_SSD e
	where not exists (select 1 from SSD.[sys].[objects] so inner join [SSD].[sys].[schemas] sc on so.schema_id = sc.schema_id where e.[object_id] = so.[object_id] or e.[object_id] is null)	-- e.entityName = so.name and e.schemaName = sc.name)

insert into dbo.entity_SSD (schemaName, entityName, [object_id])
	SELECT sch.[name] as schemaName
		,so.name as entityName
		,so.[object_id]
    FROM SSD.[sys].[objects] so
		inner join SSD.[sys].[schemas] sch on so.[schema_id] = sch.[schema_id]
	where so.type = 'U' 
		and (sch.[name] <> 'xlsi'
			or so.Name in ('IncomeFromMagazine','VendorIncome','LombardLom','IncomeRFImport','RetailTreasuryPlan','UnaccountedLocalImport'))
		and not exists (select 1 from meta.dbo.entity_SSD e where so.[object_id] = e.[object_id])
	order by entityName

update dbo.entity_SSD
	set isXLSI = 1
	where schemaName = 'xlsi'

update dbo.entity_SSD
	set isIncremental = 1
	where entityName in ('GoldRate','RemainsCollateralAssessment','ShoppingIndex','IncomeFromMagazine','VendorIncome','LombardLom','IncomeRFImport','RetailTreasuryPlan','UnaccountedLocalImport')
			
return

GO

-- EXEC [dbo].[sync_entity_SSD]