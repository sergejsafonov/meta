USE [meta]
GO
/****** Object:  StoredProcedure [dbo].[Create_DWH_relationships]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Create_DWH_relationships]
	@isOnlyDropFK bit = 0,
	@isCreatePK bit = 0,
	@schemaName nvarchar(128) = null,
	@entityName nvarchar(128) = null
AS

declare @rowCountUp int, @rowStepUp int, @rowCount int, @rowStep int, @foreignSchemaName nvarchar(128), @isColumnStore bit
declare @operateConstraint nvarchar(4000), @alterConstraint nvarchar(256), @constraintName nvarchar(128), @primaryConstraint nvarchar(4000)
declare @attributeName nvarchar(256), @foreignEntityName nvarchar(256), @foreignAttributeName nvarchar(256), @constraintFKName nvarchar(128)

-- определим количество иттераций по создаваемым таблицам для цикла
if (@schemaName is not null and @entityName is not null)
	select @rowCountUp = 1
else
	select @rowCountUp = count(*) 
		from meta.dbo.entity_DWH e
		where e.isUse = 1 and e.schemaName not in ('dbo','xls')

set @rowStepUp = 1
while (@rowStepUp <= @rowCountUp)
begin
	if (@rowStepUp = 1 and @schemaName is not null and @entityName is not null)
		select 
				  @attributeName = a.attributeName
				 ,@isColumnStore = e.isColumnStore
			from dbo.entity_DWH e
				left join dbo.attribute_DWH a on e.entityID = a.entityID and a.isUse = 1 and a.is_ID = 1
			where e.isUse = 1 and e.schemaName not in ('dbo','xls')
				and e.entityName = @entityName and e.schemaName = @schemaName

	else --if (@schemaName is null or @entityName is null)
		select
			 @schemaName = tt.schemaName
			,@entityName = tt.entityName
			,@attributeName = tt.attributeName
			,@isColumnStore = tt.isColumnStore
		from (select row_number() over (order by e.sortorder, e.EntityID) AS rowNum
					,e.schemaName
					,e.entityName
					,a.attributeName
					,e.isColumnStore
				from meta.dbo.entity_DWH e
					left join dbo.attribute_DWH a on e.entityID = a.entityID and a.isUse = 1 and a.is_ID = 1
				where e.isUse = 1 and e.schemaName not in ('dbo','xls')
			) tt where tt.rowNum = @rowStepUp
	
	set @alterConstraint = ' alter table dwh.' + @SchemaName + '.' + @entityName

	if (@isCreatePK = 1 and @attributeName is not null)
	begin
		set @constraintName = 'PK_' + @entityName

		set @primaryConstraint = 'if (select count(*) from dwh.information_schema.table_constraints where constraint_name = '''
		set @primaryConstraint = @primaryConstraint + @constraintName + ''') = 0'
		set @primaryConstraint = @primaryConstraint + @alterConstraint
		set @primaryConstraint = @primaryConstraint + ' ADD CONSTRAINT ' + @constraintName + ' PRIMARY KEY'	

--		if (@isColumnStore = 0)
--			set @primaryConstraint = @primaryConstraint + ' CLUSTERED'

		set @primaryConstraint = @primaryConstraint + ' (' + @attributeName + ')'

--		select @primaryConstraint
		exec (@primaryConstraint)
	end

	select @rowCount = count(*) 
		from meta.dbo.attribute_DWH a
			inner join meta.dbo.attribute_DWH fa on a.relAttributeID = fa.attributeID and fa.is_ID = 1
		where a.entityName = @entityName
			and a.schemaName = @schemaName
			and a.isUse = 1 and a.schemaName not in ('dbo','xls') and fa.attributeName is not null

	if (@rowCount = 0 and @rowStepUp < @rowCountUp)
	begin
		set @rowStepUp = @rowStepUp + 1	
		continue
	end
	else if (@rowCount = 0 and @rowCountUp = 1)
	begin
		select 'Нет необходимости создавать внешние ключи для таблицы dwh.' + @SchemaName + '.' + @entityName + ' !' as operateConstraint
		break
	end

	set @rowStep = 1
	-- цикл определения признаков столбцов для создания внешних ключей таблицы
	while (@rowStep <= @rowCount)
	begin
		select 
				 @attributeName = tt.attributeName
				,@foreignSchemaName = tt.foreignSchemaName
				,@foreignEntityName = tt.foreignEntityName
				,@foreignAttributeName = tt.foreignAttributeName
			from (
				select row_number() over (order by a.attributeID) AS rowNum
					,a.attributeName
					,fa.schemaName as foreignSchemaName
					,fa.entityName as foreignEntityName
					,fa.attributeName as foreignAttributeName
				from meta.dbo.attribute_DWH a
					inner join meta.dbo.attribute_DWH fa on a.relAttributeID = fa.attributeID and fa.is_ID = 1 and fa.attributeName is not null and fa.isUse = 1
				where a.entityName = @entityName
					and a.schemaName = @schemaName
					and a.isUse = 1 and a.schemaName not in ('dbo','xls')
					and a.is_ID = 0
				) tt where tt.rowNum = @rowStep

		if (@attributeName is not null)
		begin			
			set @constraintFKName = 'FK_' + @entityName + '_' + @attributeName
			set @operateConstraint = ''

			if (@isOnlyDropFK = 1)
			begin
				set @operateConstraint = @operateConstraint + 'if exists (select 1 from dwh.information_schema.table_constraints where constraint_name = '''
				set @operateConstraint = @operateConstraint + @constraintFKName + ''')'
				set @operateConstraint = @operateConstraint + @alterConstraint
				set @operateConstraint = @operateConstraint + ' drop constraint ' + @constraintFKName
			end
			
			set @operateConstraint = @operateConstraint + ' if (select count(*) from dwh.information_schema.table_constraints where constraint_name = '''
			set @operateConstraint = @operateConstraint + @constraintFKName + ''') = 0'
			set @operateConstraint = @operateConstraint + @alterConstraint
			set @operateConstraint = @operateConstraint + ' add constraint ' + @constraintFKName
			set @operateConstraint = @operateConstraint + ' foreign key (' + @attributeName
			set @operateConstraint = @operateConstraint + ') references dwh.' + @foreignSchemaName + '.'
			set @operateConstraint = @operateConstraint + @foreignEntityName + ' (' + @foreignAttributeName + ')'
		
--			select @operateConstraint
			exec (@operateConstraint)
		end

		set @rowStep = @rowStep + 1	
	end

	set @rowStepUp = @rowStepUp + 1	
end


GO
