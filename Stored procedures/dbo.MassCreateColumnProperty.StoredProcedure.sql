USE [meta]
GO
/****** Object:  StoredProcedure [dbo].[MassCreateColumnProperty]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--AG
--для массовых замен ставить '' или null
CREATE Procedure [dbo].[MassCreateColumnProperty](
@db nvarchar(100) = 'dwh', @Schema nvarchar(20) = 'sap',@table nvarchar(100), @Column nvarchar(100), @Attr nvarchar(255), @DefaultValue nvarchar(max) = '' ) 
as begin

DEclare @Result Table(db nvarchar(100), [Schema] nvarchar(20),[table] nvarchar(100), [status] nvarchar(max))

DEclare @sql nvarchar(max);
-- Check Entries
If @db is null or (Select top 1 d.database_id from sys.databases d Where d.name = isnull(@db,'')) is null 
	begin
	Print 'database is empty or not found'
	Print 'DB doesn`t OK'
	Insert @Result VALUES(NULL,NULL,NULL,'db name is empty or not found');
	return;
	end;
Print 'DB OK'

If isnull(@Schema,'') = '' 
begin
	Print 'Schema is null or empty. MultiSchema'
end
Else
begin 
Set @sql =
	'If not exists (Select s.schema_id from '+@db+'.sys.schemas s Where s.name = '''+isnull(@Schema,'')+''') 
	begin
		Print(''schema name not found'') 
	end'
Begin Try
	Exec(@sql);
End Try
Begin CATCH
	Print 'Schema is not found '
	return;
END CATCH
end
Print 'Schema OK'
If isnull(@table,'') ='' 
begin
	Print 'Table is null or empty. MultiTable'
end
Else
begin
Set @sql= '
If (Select top 1 t.object_id from '+@db+'.sys.tables t Where  t.name = '''+isnull(@table,'')+''' and schema_id = (Select top 1 Schema_id from '+@db+'.sys.schemas Where name = '''+@Schema+''')) is null 
	begin
	Print(''Table found'')
	end;'
Begin Try 
	Exec(@sql);
end Try
Begin catch
	Print 'Table not found'
	Print @sql;
	return;
end catch
Print 'Table OK'
end

If isnull(@Column,'') ='' 
begin
	Print 'Column is null or empty. MultiColumn'
end
Else
begin
/*Set @sql= 'Use '+@db+
'; 	
	If (Select top 1 c.column_id from '+@db+'.sys.columns c Where c.name = '''+isnull(@Column,'')+''' ) is null 
	begin
	Print ''Column not found''
	end;'
Begin Try 
	Exec(@sql);
end Try
Begin catch
	Print 'Column not found'
	Print @sql
	return;
end catch*/
Print 'Column OK'
end

If isnull(@Attr,'') = ''
Begin
	Print 'Attr is null or empty'
	Print 'Attr doesn`t OK'
	Return
End
Else Print 'Attribute OK'
-- Prepare schema+table+column set
Create Table #Tbl(id int, Schema_name nvarchar(100), Table_name nvarchar(100),Column_Name nvarchar(100));
SET @sql =--'Use '+@db+';  
'
	insert into #tbl
	Select row_number() over(Order by s.name, t.name, c.name) as id, s.name as sname,t.name as tname, c.name as cname    
	from '+@db+'.sys.schemas s   
	inner join '+@db+'.sys.tables t on t.schema_id = s.schema_id 
	inner join '+@db+'.sys.columns c on c.object_id = t.object_id
	Where s.[name] like ''%'+isnull(@Schema,'')+'%'' and t.[name] like ''%'+isnull(@table,'')+'%'' and c.[name] like ''%'+isnull(@Column,'')+'%''
'
--Print @sql
Exec (@sql);
--Select * from #tbl;
--Setting Attribute
DEclare @RowCnt int = 1;
Declare @procedureName nvarchar(100);
Declare @colname nvarchar(100), @tblname nvarchar(100), @schname nvarchar(100) 
Create Table #ProcFlag(Proc_Name varchar(30))
Select *,@Attr from #Tbl
While @RowCnt<=(Select count(id) from #Tbl)
begin
	Select @schname= tt.Schema_name, @tblname = tt.Table_name, @colname = tt.Column_Name from #Tbl tt Where @RowCnt = tt.id
	Set @sql = '
	use '+@db+';
	if (exists (SELECT * FROM fn_listextendedproperty ('''+@Attr+''', ''Schema'', '''+@Schname+''', ''Table'', '''+@tblname+''', ''Column'', '''+@Colname+''')))			
			insert #ProcFlag Select ''sp_updateextendedproperty''			
			else insert #ProcFlag Select  ''sp_addextendedproperty'';'
	Exec(@sql);
	Set @procedureName = (Select top 1 Proc_Name from #ProcFlag);
	delete from #ProcFlag;
	set @sql = 	@ProcedureName + ' @name = '+isnull(@Attr,'Description')+',@value = N''' + isnull(@DefaultValue,'') + '''' 
	+ ', @level0type = N''Schema'',@level0name = N''' + isnull(@Schname,'') + ''''
	+ ', @level1type = N''Table'', @level1name = N''' + isnull(@tblname,'') + ''''
	+ ', @level2type = N''Column'',@level2name = N''' + isnull(@colname,'') + ''''
	Set @sql = 'Use '+@db+ ';' + 'Exec '+ @sql;
	Begin Try
	exec (@sql)
	End Try
	begin catch
	Print 'Add|Update property error: '+@db+'.'+@schname+'.'+@tblname+'.'+@colname+':'+@Attr
	Print @sql
	end catch
	Set @RowCnt = @RowCnt + 1
end;
drop table #tbl;
drop table #procflag;
End/**/

--Exec [dbo].[MassCreateColumnProperty] 'dwh','sap','','BusinessAreaID','Caption','Бизнес-сфера'


GO
