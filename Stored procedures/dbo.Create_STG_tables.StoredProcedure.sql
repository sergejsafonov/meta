USE [meta]
GO
/****** Object:  StoredProcedure [dbo].[Create_STG_tables]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Create_STG_tables]
	@schemaName nvarchar(128) = null,
	@startEntityName nvarchar(128) = null
AS

declare @rowCountUp int, @rowStepUp int, @rowCount int, @rowStep int, @collation nvarchar(128), @isFullDelete bit, @isPK bit
declare @dropTable nvarchar(max), @createTable nvarchar(max), @entityID int, @entityName nvarchar(128)
declare @attributeName nvarchar(128), @typeName nvarchar(32), @is_nullable bit, @defaultValue nvarchar(256)

-- определим количество иттераций по создаваемым таблицам для цикла
if (@startEntityName is not null and @schemaName is not null)
	select @rowCountUp = 1

else if (@startEntityName is null and @schemaName is not null)
	select @rowCountUp = count(*) 
		from dbo.entity_STG e
		where e.schemaName = @schemaName and e.isUse = 1

else
	select @rowCountUp = count(*) 
		from dbo.entity_STG e
		where e.isUse = 1

set @rowStepUp = 1
while (@rowStepUp <= @rowCountUp)
begin
	if (@rowStepUp = 1 and @schemaName is not null and @startEntityName is not null)
		select 
				 @entityID = e.entityID
				,@entityName = e.entityName
				,@isFullDelete = e.isFullDelete
			from dbo.entity_STG e
			where e.schemaName = @schemaName and e.entityName = @startEntityName and e.isUse = 1

	else if (@startEntityName is null and @schemaName is not null)
		select
			 @entityID = tt.entityID 
			,@entityName = tt.entityName
			,@isFullDelete = tt.isFullDelete
		from (select row_number() over (order by e.entityID) AS rowNum
					,e.entityID
					,e.entityName
					,e.isFullDelete
				from dbo.entity_STG e
				where e.schemaName = @schemaName and e.isUse = 1 
			) tt where tt.rowNum = @rowStepUp
	else
		select
			 @entityID = tt.entityID 
			,@entityName = tt.entityName
			,@schemaName = tt.schemaName
			,@isFullDelete = tt.isFullDelete
		from (select row_number() over (order by e.entityID) AS rowNum
					,e.entityID
					,e.entityName
					,e.schemaName
					,e.isFullDelete
				from dbo.entity_STG e
				where e.isUse = 1 
			) tt where tt.rowNum = @rowStepUp
		
	set @dropTable = 'use stg if object_id(''' + @schemaName + '.' + @entityName + 
		''') is not null drop table ' + @schemaName + '.' + @entityName

	set @createTable = 'create table STG.' + @schemaName + '.' + @entityName + ' ('
	set @createTable = @createTable + 'UID int not null identity(1,1) primary key clustered,'

	select @rowCount = count(*) 
		from dbo.attribute_STG a
			inner join dbo.entity_STG e on a.entityID = e.entityID
		where a.isUse = 1 and e.schemaName = @schemaName and a.entityName = @entityName 

	set @rowStep = 1
	-- цикл определения признаков столбцов для создания таблицы
	while (@rowStep <= @rowCount)
	begin
		select 
				 @attributeName = '[' + tt.attributeName + ']'
				,@typeName = tt.typeName
				,@is_nullable = tt.is_nullable
				,@defaultValue = tt.defaultValue
				,@collation = tt.collation
				,@isPK = tt.isPK
			from (
				select row_number() over (order by a.columnID) AS rowNum,
					a.attributeName, 
					a.typeName, 
					a.is_nullable, 
					a.defaultValue,
					a.isPK,
					isnull('COLLATE ' + a.collation,'') collation
				from dbo.attribute_STG a
					inner join dbo.entity_STG e on a.entityID = e.entityID
				where a.isUse = 1 and e.schemaName = @schemaName and a.entityName = @entityName 
			) tt where tt.rowNum = @rowStep

		-- определим строку для каждого столбца
		set @createTable = @createTable + @attributeName + ' ' + @typeName + ' ' + @collation + ' ' +
			(case when @is_nullable = 1 or @isFullDelete = 1 and @isPK = 0 then '' else 'not ' end) + 'null' + 
			(case when @defaultValue is not null then ' default(' + @defaultValue + ')' else '' end)

		if (@rowStep < @rowCount)
			set @createTable = @createTable + ','

		set @rowStep = @rowStep + 1	
	end

	set @createTable = @createTable + ') ON [PRIMARY]'

	exec (@dropTable)	
	
	-- скрипт создания таблицы
--	select @createTable
	exec (@createTable)

	set @rowStepUp = @rowStepUp + 1	
end


GO
