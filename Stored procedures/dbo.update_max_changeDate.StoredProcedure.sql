USE [meta]
GO
/****** Object:  StoredProcedure [dbo].[update_max_changeDate]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[update_max_changeDate]
	@schemaName nvarchar(128) = null,
	@entityName nvarchar(128) = null
AS

declare @rowCount int, @rowStep int, @entityID int, @attributeChangeDate nvarchar(128), @attributeChangeDateTime nvarchar(128), @attributeChangeCode nvarchar(128)
declare @updateStr nvarchar(max)

if (@schemaName is null and @entityName is not null or @schemaName not in ('sap','pos','dbo','xls','crm'))
begin
	raiserror('Не указана @schemaName!',16,1)
	return
end

begin try
	insert into dbo.max_changeDate (entityID,schemaName,entityName,sortOrder,attributeChangeDate,attributeChangeDateTime,attributeChangeCode)
		select 
			e.entityID,
			e.schemaName,
			e.entityName,
			e.sortOrder,
			a.attributeName as attributeChangeDate,
			a1.attributeName as attributeChangeDateTime,
			a2.attributeName as attributeChangeCode
		from dbo.entity_DWH e
			left outer join dbo.attribute_DWH a on e.entityID = a.entityID and a.isChangeDate = 1
			left outer join dbo.attribute_DWH a1 on e.entityID = a1.entityID and a1.isChangeDateTime = 1
			left outer join dbo.attribute_DWH a2 on e.entityID = a2.entityID and a2.isChangeCode = 1
		where e.isUse = 1 and e.isIncremental = 1 and (a.isUse = 1 and a.isChangeDate = 1 or a1.isUse = 1 and a1.isChangeDateTime = 1 or a2.isUse = 1 and a2.isChangeCode = 1)
			and not exists (select 1 from dbo.max_changeDate mcd where mcd.entityID = e.entityID)

	-- определим количество иттераций по создаваемым таблицам для цикла
	if (@entityName is not null)
		select @rowCount = 1
	else
		select @rowCount = count(*) 
			from dbo.max_changeDate mcd
		
	set @rowStep = 1
	while (@rowStep <= @rowCount)
	begin
		if (@rowStep = 1 and @entityName is not null)
			select 
					 @entityID = entityID
					,@schemaName = schemaName
					,@attributeChangeDate = attributeChangeDate
					,@attributeChangeDateTime = attributeChangeDateTime
					,@attributeChangeCode = attributeChangeCode
				from dbo.max_changeDate
				where entityName = @entityName and schemaName = @schemaName
		else
			select
				 @entityID = tt.entityID 
				,@entityName = tt.entityName
				,@schemaName = tt.schemaName
				,@attributeChangeDate = tt.attributeChangeDate
				,@attributeChangeDateTime = tt.attributeChangeDateTime
				,@attributeChangeCode = tt.attributeChangeCode
			from (select row_number() over (order by sortOrder, entityID) AS rowNum
						,entityID
						,entityName
						,schemaName
						,attributeChangeDate
						,attributeChangeDateTime
						,attributeChangeCode
					from dbo.max_changeDate
				) tt where tt.rowNum = @rowStep
	
		set @updateStr = 'update dbo.max_changeDate set '

		if (@attributeChangeDate is not null)
		begin
			set @updateStr = @updateStr + 'MaxChangeDate = (select max(' +  @attributeChangeDate + ') from dwh.' + @schemaName + '.' + @entityName + ')'

			if (@attributeChangeDateTime is not null)
				set @updateStr = @updateStr + ','
		end

		if (@attributeChangeDateTime is not null)
		begin
			set @updateStr = @updateStr + 'MaxChangeDateTimeID = (select max(' + @attributeChangeDateTime + ') from dwh.' + @schemaName + '.' + @entityName + ')'

			if (@attributeChangeCode is not null)
				set @updateStr = @updateStr + ','
		end

		if (@attributeChangeCode is not null)
			set @updateStr = @updateStr + 'MaxChangeCode = (select max(' + @attributeChangeCode + ') from dwh.' + @schemaName + '.' + @entityName + ')'

		set @updateStr = @updateStr + ' where schemaName = ''' + @schemaName + ''' and entityName = ''' + @entityName + ''''
		
	--	select @updateStr
		exec (@updateStr)
	
		set @rowStep = @rowStep + 1	
	end
end try

begin catch
	if (@@ERROR <> 4701)
		throw;

	select ERROR_MESSAGE()
end catch


GO
