USE [meta]
GO
/****** Object:  StoredProcedure [dbo].[ODSLoadFileToStg]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ODSLoadFileToStg]
	@typeLoad nvarchar(32) = 'incremental',
	@Name nvarchar(32) = null
as

declare @rowStep int, @rowCount int, @ODSInsertStr nvarchar(max), @resultEntity nvarchar(512), @ConnectionString nvarchar(1024), @SheetName nvarchar(64)
declare @deleteFile nvarchar(512), @fileExistsFile nvarchar(512), @localHost nvarchar(max), @DropTableStr nvarchar(max), @isIncremental bit 

if (@typeLoad = 'incremental')
	set @isIncremental = 1
else
	set @isIncremental = 0

if (@Name is not null)
	set @rowCount = 1
else	
	select @rowCount = count(*)
		from dbo.fileTransfer ft
		where ft.isXLS = 1 
			and ft.isReverse = 0 
			and ft.isIncremental = @isIncremental 
			and ft.isUse = 1

set @rowStep = 1
while (@rowStep <= @rowCount)
begin
	select 
		@Name = tt.Name,
		@localHost = tt.localHost,
		@resultEntity = tt.resultEntity , 
		@ConnectionString = tt.ConnectionString, 
		@SheetName = tt.SheetName
	from (select row_number() over (order by ft.fileTransferID) AS rowNum,
			ft.Name,
			ft.localHost,
			ft.resultEntity,
			ft.ConnectionString,
			ft.SheetName
		from dbo.fileTransfer ft
		where ft.isXLS = 1 
			and ft.isReverse = 0 
			and (ft.Name = @Name or @Name is null)
			and ft.isIncremental = @isIncremental 
			and ft.isUse = 1
		) tt where tt.rowNum = @rowStep

	begin try
		set @fileExistsFile = 'declare @isExists int exec master.dbo.xp_fileexist ''' + @localHost
		set @fileExistsFile = @fileExistsFile + ''', @isExists OUTPUT if (@isExists = 1) select @isExists'

	--	select @fileExistsFile
		exec (@fileExistsFile)

		if (@@rowcount > 0)
		begin
			begin transaction
				set @DropTableStr = 'IF OBJECT_ID(''' + @resultEntity + ''') is not null DROP TABLE ' + @resultEntity

--				select @DropTableStr
				exec (@DropTableStr)

				set @ODSInsertStr = 'SELECT * INTO ' + @resultEntity + ' FROM OPENDATASOURCE(' + @ConnectionString + ')...[' + @SheetName + '$]' 
			
--				select @ODSInsertStr
				exec (@ODSInsertStr)
		/*
				set @deleteFile = 'master..xp_cmdshell ''del ' + @localHost + ''''

				select @deleteFile
		--		exec (@deleteFile)
		*/
				update dbo.fileTransfer
					set DateOperation = getdate(),
						LoadCount = @@ROWCOUNT,
						errorMessage = null
					where Name = @Name	

			commit transaction
		end
	end try

	begin catch
		rollback transaction

		update dbo.fileTransfer
			set DateOperation = getdate(),
				LoadCount = null,
				errorMessage = ERROR_MESSAGE()
			where Name = @Name
	end catch

	set @Name = null

	set @rowStep = @rowStep + 1	
end			

return

GO
