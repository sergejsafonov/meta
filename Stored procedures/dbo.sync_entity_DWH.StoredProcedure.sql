USE [meta]
GO

alter PROCEDURE [dbo].[sync_entity_DWH]
AS

update e
	set [object_id] = so.[object_id]
	from dbo.entity_DWH e
		inner join [DWH].[sys].[objects] so on e.entityName = so.name
		inner join [DWH].[sys].[schemas] sc on so.schema_id = sc.schema_id and sc.[name] = e.schemaName
	where (e.[object_id] <> so.[object_id] or e.[object_id] is null) 

update e
	set entityName = so.name
	from dbo.entity_DWH e
		inner join [DWH].[sys].[objects] so on e.[object_id] = so.[object_id]
		inner join [DWH].[sys].[schemas] sc on so.schema_id = sc.schema_id and sc.[name] = e.schemaName
	where e.entityName <> so.name 

delete from a
	from dbo.attribute_DWH a
		inner join dbo.entity_DWH e on a.entityID = e.entityID
	where not exists (select 1 from DWH.[sys].[columns] sc
						 where sc.[object_id] = e.[object_id] and sc.name = a.attributeName)

delete from e
	from dbo.entity_DWH e
	where not exists (select 1 from [DWH].[sys].[objects] so inner join [DWH].[sys].[schemas] sc on so.schema_id = sc.schema_id where e.[object_id] = so.[object_id] or e.[object_id] is null)		-- e.entityName = so.name and e.schemaName = sc.name)
		and not exists (select 1 from dbo.attribute_DWH a where a.entityID = e.entityID)

delete from e
	from dbo.entity_DWH e
	where not exists (select 1 from [DWH].[sys].[objects] so inner join [DWH].[sys].[schemas] sc on so.schema_id = sc.schema_id where e.entityName = so.name and sc.[name] = e.schemaName)		-- e.entityName = so.name and e.schemaName = sc.name)
		and not exists (select 1 from dbo.attribute_DWH a where a.entityID = e.entityID)

insert into dbo.entity_DWH (schemaName, entityName, [object_id], sortOrder)
	SELECT sch.[name] as schemaName
		,so.name as entityName
		,so.[object_id]
		,0 as sortOrder
	FROM [DWH].[sys].[objects] so
		inner join [DWH].[sys].[schemas] sch on so.[schema_id] = sch.[schema_id]
	where so.type = 'U' 
		and so.name not in ('Session','sysdiagrams')
		and not exists (select 1 from meta.dbo.entity_DWH e inner join [DWH].[sys].[schemas] sch on sch.[name] = e.schemaName where so.[object_id] = e.[object_id])
	order by so.[object_id]

insert into dbo.attribute_DWH (entityID,entityName,schemaName,attributeName,columnID,typeName,is_nullable,defaultValue,is_ID,is_Code,is_Composite,is_FlagOperation,
		is_DateOperation,FlagDML)
	SELECT 
	   e.entityID
	  ,e.entityName
	  ,e.schemaName
      ,sc.name as attributeName
      ,sc.column_id as columnID
	   ,(case when st.name = 'nvarchar' then st.name + '(' + cast(sc.[max_length]/2 as nvarchar(4)) + ')' 
			when st.name = 'varchar' then 'n' + st.name + '(' + cast(sc.[max_length]/2 as nvarchar(4)) + ')' 
			when st.name in ('decimal','numeric') then st.name + '(13,3)' 
			else st.name end) as typeName
      ,sc.[is_nullable]
	  ,(case when sc.name = 'FlagOperation' then '1' 
			when sc.name = 'DataOperation' then 'getdate()' 
			else null end) as defaultValue
      ,(case when sc.name = e.entityName + 'ID' 
				or sc.name = 'Type' and e.entityName = 'CodeType'
				or sc.name = 'EntityName' and e.entityName = 'SlidingDate'
			then 1 else 0 end) as is_ID
	  ,(case when sc.name = 'Code' and e.entityName not in ('CodeType','Characteristic')
			or sc.Name = 'DBsourceID'
			or e.entityName = 'Sklad' and sc.name in ('PredpriyatieCode','SkladCode') 
			or e.entityName = 'Product' and sc.name = 'ProductHash'
			or e.entityName = 'Kassir' and sc.name = 'ShopCode'
			or e.entityName = 'GiftCardPosition' and sc.name = 'PositionCode'
			or e.entityName in ('CostCenter','ProfitCenter') and sc.name = 'DateEnd'
		then 1 else 0 end) as is_Code
	  ,(case sc.name when 'Composite' then 1 else 0 end) as is_Composite
	  ,(case sc.name when 'FlagOperation' then 1 else 0 end) as is_FlagOperation
	  ,(case sc.name when 'DateOperation' then 1 else 0 end) as is_DateOperation
	  ,0 as flagDML
	FROM [DWH].[sys].[columns] sc
		inner join dbo.entity_DWH e on sc.[object_id] = e.[object_id]
		inner join [DWH].[sys].[types] st on sc.[system_type_id] = st.[system_type_id]
	where st.name <> 'sysname' 
		and not exists (select 1 from meta.dbo.attribute_DWH a where e.entityID = a.entityID and sc.name = attributeName)
	order by entityName,columnID

return

GO

-- EXEC [dbo].[sync_entity_DWH]