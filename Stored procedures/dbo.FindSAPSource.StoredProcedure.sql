USE [meta]
GO
/****** Object:  StoredProcedure [dbo].[FindSAPSource]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--Exec [dbo].[FindSAPSource] 'Брак'

CREATE  PROCEDURE [dbo].[FindSAPSource]
@CaptionToFind nvarchar(128) = ''
As

Select distinct d4l.RollName,d4t.DDTEXT, d4t.REPTEXT, d4t.SCRTEXT_L,
d3l.TABNAME, d3l.FIELDNAME, d3l.REFTABLE, d3l.REFFIELD, d3t.DDTEXT

from stg.sap.dd04l d4l
left join stg.sap.dd04T d4t on d4t.ROLLNAME = d4l.ROLLNAME and d4l.AS4LOCAL = d4t.AS4LOCAL and d4l.as4vers = d4t.AS4VERS
inner join stg.sap.dd03l d3l on d3l.AS4LOCAL = d3l.AS4LOCAL and d3l.AS4VERS = d3l.AS4VERS and d3l.ROLLNAME = d4l.RollName
left join stg.sap.dd03t d3t on d3t.TABNAME = d3l.TABNAME and d3t.FIELDNAME = d3l.FIELDNAME and d3t.AS4LOCAL = d3l.AS4LOCAL
Where 
D4T.DDLanguage = 'R' 
and PATINDEX('%/%', convert(nvarchar(255),d3l.TabName)) =0 and PATINDEX('%/%', convert(nvarchar(255),d4l.ROLLNAME)) =0
and d3l.TABNAME <> d3l.REFTABLE
and ((isnull(d4t.DDTEXT,'') like '%'+@CaptionToFind+'%' or isnull(d3t.DDTEXT,'') like '%'+@CaptionToFind+'%') 
or d4t.REPTEXT like '%'+@CaptionToFind+'%' or d4t.SCRTEXT_L like '%'+@CaptionToFind+'%')

Order by 1, TABNAME



--Exec [dbo].[FindSAPSource] 'ГЛавн'




GO
