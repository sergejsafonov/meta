USE [meta]
GO
/****** Object:  StoredProcedure [dbo].[reset_flagOperation_ssd]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[reset_flagOperation_ssd]
	@schemaName nvarchar(128) = null,
	@entityName nvarchar(128) = null
AS

declare @rowCount int, @rowStep int, @entityID int, @updateStr nvarchar(max), @deleteStr nvarchar(max), @isFact bit

if (@schemaName is null and @entityName is not null or @schemaName not in ('txt','xls','xlsx','csv','xlsi','json'))
begin
	raiserror('Не указана @schemaName!',16,1)
	return
end

begin try
	SET CONTEXT_INFO 0x0101010101;

	-- определим количество иттераций по создаваемым таблицам для цикла
	if (@entityName is not null)
		select @rowCount = 1
	else
		select @rowCount = count(*) 
			from dbo.entity_SSD e
			where e.isUse = 1 and e.isIncremental = 1
		
	set @rowStep = 1
	while (@rowStep <= @rowCount)
	begin
		if (@rowStep = 1 and @entityName is not null)
			select 
					 @entityID = e.entityID,
					 @schemaName = e.schemaName,
					 @isFact = e.isFact
				from dbo.entity_SSD e
				where e.isUse = 1 and e.isIncremental = 1 and e.entityName = @entityName and e.schemaName = @schemaName
		else
			select
				 @entityID = tt.entityID 
				,@entityName = tt.entityName
				,@schemaName = tt.schemaName
				,@isFact = tt.isFact
			from (select row_number() over (order by e.sortOrder desc, e.entityID) AS rowNum
						,e.entityID
						,e.entityName
						,e.schemaName
						,e.isFact
					from dbo.entity_SSD e
					where e.isUse = 1 and e.isIncremental = 1
				) tt where tt.rowNum = @rowStep
					
		if (@isFact = 1)
		begin
			set @deleteStr = 'delete from SSD.[' + @schemaName + '].[' + @entityName + '] where FlagOperation = 3'

	--		select @deleteStr
			exec (@deleteStr)
		end

		set @updateStr = 'update SSD.[' + @schemaName + '].[' + @entityName + '] set FlagOperation = 0 where FlagOperation in (1,2,4,5)'

--		select @updateStr
		exec (@updateStr)

		set @rowStep = @rowStep + 1	
	end
end try

begin catch
	if (@@ERROR <> 4701)
		throw;

	select ERROR_MESSAGE()
end catch


GO

-- exec [dbo].[reset_flagOperation_ssd]