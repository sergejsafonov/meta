USE [meta]
GO
/****** Object:  StoredProcedure [dbo].[truncate_SST_tables]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[truncate_SST_tables]
	@isXLSI bit =null,
	@schemaName nvarchar(128) = null,
	@entityName nvarchar(128) = null
	
AS

declare @rowCount int, @rowStep int, @selectStr nvarchar(max), @entityID int

begin try
	-- определим количество иттераций по создаваемым таблицам для цикла
	if (@entityName is not null and @schemaName is not null)
		select @rowCount = 1
	else
		select @rowCount = count(*) 
			from dbo.entity_SST e
				left join dbo.fileTransfer ft on e.schemaName = ft.schemaName and e.entityName = ft.entityName and ft.isUse = 1
			where e.isUse = 1 
				and (ft.fileTransferID is not null or e.isXLSI = @isXLSI)

	set @rowStep = 1
	while (@rowStep <= @rowCount)
	begin
		if (@rowStep = 1 and @entityName is not null and @schemaName is not null)
			select 
					 @entityID = e.entityID
				from dbo.entity_SST e
					left join dbo.fileTransfer ft on e.schemaName = ft.schemaName and e.entityName = ft.entityName and ft.isUse = 1
				where e.isUse = 1 
					and (ft.fileTransferID is not null or e.isXLSI = @isXLSI)
					and e.entityName = @entityName 
					and e.schemaName = @schemaName 
		else
			select
				 @entityID = tt.entityID 
				,@entityName = tt.entityName
				,@schemaName = tt.schemaName
			from (select row_number() over (order by e.sortOrder desc, e.entityID) AS rowNum
						,e.entityID
						,e.entityName
						,e.schemaName
					from dbo.entity_SST e
						left join dbo.fileTransfer ft on e.schemaName = ft.schemaName and e.entityName = ft.entityName and ft.isUse = 1
					where e.isUse = 1 
						and (ft.fileTransferID is not null or e.isXLSI = @isXLSI)
				) tt where tt.rowNum = @rowStep
	
	--	if (@isPOS = 1)
	--		set @selectStr = 'use stg IF OBJECT_ID(''' + @schemaName + '.' + @entityName + ''') is not null drop table STG_SSD.' + @schemaName + '.' + @entityName
	--	else
		set @selectStr = 'truncate table STG_SSD.[' + @schemaName + '].' + @entityName

		exec (@selectStr)
--		select @selectStr
		
		set @rowStep = @rowStep + 1	
	end
end try

begin catch
	if (@@ERROR <> 4701)
		throw;

	select ERROR_MESSAGE()
end catch


GO
