USE [meta]
GO
/****** Object:  StoredProcedure [dbo].[get_selectFromSource]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[get_selectFromSource]
	@schemaName NVARCHAR(128) = NULL,
	@entityName NVARCHAR(128) = NULL,
	@typeLoad NVARCHAR(32) = 'incremental',
	@selectStr NVARCHAR(MAX) = '' OUTPUT
AS

declare @rowCountUp int, @rowStepUp int, @rowCount int, @rowStep int, @entityID int, @addQueryStr nvarchar(max), @alias nvarchar(8), @existsClause nvarchar(max)
declare @attributeName nvarchar(128), @is_slidingDate bit, @slidingClause nvarchar(max), @refEntityName nvarchar(128), @refAttributeName nvarchar(128), @isFullDelete bit
declare @refAlias nvarchar(8), @refSlidingDate nvarchar(128), @SlidingDateParam nvarchar(32), @isPOS bit, @whereClause nvarchar(max), @isFact bit, @sourceSchema nvarchar(256)
declare @isIncremental bit, @isNightFull bit, @isWeekFull bit, @isAddQueryStrIncremental bit, @clientField nvarchar(32), @isCRM bit, @MaxChangeDate nchar(8), @isPK bit
declare @isPOSSet bit, @isPOSLoy bit, @isAddPK bit, @isUseSlidingDate bit, @MaxChangeCode nvarchar(32), @schemaAlias nvarchar(128), @isEveryHour bit, @is_slidingDate2 bit
declare @AddSlidingClause nvarchar(max), @isUse bit, @atype nvarchar(50), @atype2 nvarchar(50), @isTimeStamp bit
;
-- определим количество иттераций по создаваемым таблицам для цикла
if (@entityName is not null)
	select @rowCountUp = 1
ELSE
	SELECT @rowCountUp = COUNT(*) 
		FROM dbo.entity_STG e
--		where e.isUse = 1

set @addQueryStr = ''
set @sourceSchema = ''

set @rowStepUp = 1
WHILE (@rowStepUp <= @rowCountUp)
BEGIN
	if (@rowStepUp = 1 and @entityName is not null)
		select 
				 @entityID = e.entityID,
				 @alias = e.alias,
				 @schemaAlias = e.schemaAlias,
				 @addQueryStr = isnull(e.addQueryStr,''),
				 @isIncremental = e.isIncremental,
				 @isNightFull = e.isNightFull,
				 @isWeekFull = e.isWeekFull,
				 @isEveryHour = e.isEveryHour,
				 @isFullDelete = e.isFullDelete,
				 @isAddQueryStrIncremental = e.isAddQueryStrIncremental,
				 @isPOS = e.isPOS,
				 @isPOSSet = e.isPOSSet,
				 @isPOSLoy = e.isPOSLoy,
				 @isCRM = e.isCRM,
				 @isFact = e.isFact,
				 @clientField = e.clientField,
				 @isUseSlidingDate = e.isUseSlidingDate,
				 @isUse = e.isUse
			from dbo.entity_STG e
			where e.schemaName = @schemaName 
				and e.entityName = @entityName 
--				and e.isUse = 1
	ELSE
		SELECT
			 @entityID = tt.entityID 
			,@entityName = tt.entityName
			,@alias = tt.alias
			,@schemaAlias = tt.schemaAlias
			,@addQueryStr = tt.addQueryStr
			,@isIncremental = tt.isIncremental
			,@isNightFull = tt.isNightFull
			,@isWeekFull = tt.isWeekFull
			,@isEveryHour = tt.isEveryHour
			,@isFullDelete = tt.isFullDelete
			,@isAddQueryStrIncremental = tt.isAddQueryStrIncremental
			,@schemaName = tt.schemaName
			,@isPOS = tt.isPOS
			,@isPOSSet = tt.isPOSSet
			,@isPOSLoy = tt.isPOSLoy
			,@isCRM = tt.isCRM
			,@isFact = tt.isFact
			,@clientField = tt.clientField
			,@isUseSlidingDate = tt.isUseSlidingDate
			,@isUse = tt.isUse
		FROM (SELECT ROW_NUMBER() OVER (ORDER BY e.sortOrder, e.entityID) AS rowNum
					,e.entityID
					,e.entityName
					,ISNULL(e.alias,e.entityName) alias
					,e.schemaAlias
					,ISNULL(e.addQueryStr,'') addQueryStr
					,e.isIncremental
					,e.isNightFull
					,e.isWeekFull
					,e.isEveryHour
					,e.isFullDelete
					,e.isAddQueryStrIncremental
					,e.schemaName
					,e.isPOS
					,e.isPOSSet
					,e.isPOSLoy
					,e.isCRM
					,e.isFact
					,e.clientField
					,e.isUseSlidingDate
					,e.isUse
				FROM dbo.entity_STG e
--				where e.isUse = 1
			) tt WHERE tt.rowNum = @rowStepUp
	
	if (@schemaAlias <> '')
		set @sourceSchema = @schemaAlias + '.'

	set @selectStr = 'select '
	set @whereClause = ' where '
	set @existsClause = ''
	set @slidingClause = ''

	select @rowCount = count(*) 
		from dbo.attribute_STG a
			inner join dbo.entity_STG e on a.entityID = e.entityID
		where a.isUse = 1 and e.schemaName = @schemaName and e.entityName = @entityName

	set @rowStep = 1
	-- цикл определения признаков столбцов для создания таблицы
	while (@rowStep <= @rowCount)
	begin
		select 
					@attributeName = tt.attributeName,
					@isPK = tt.isPK,
					@isAddPK = tt.isAddPK,
					@is_slidingDate = tt.is_slidingDate,
					@is_slidingDate2 = tt.is_slidingDate2,
					@refEntityName = tt.refEntityName,
					@refAlias = tt.refAlias,
					@refAttributeName = tt.refAttributeName,
					@refSlidingDate = tt.refSlidingDate,
					@MaxChangeDate = tt.MaxChangeDate,
					@MaxChangeCode = tt.MaxChangeCode,
					@atype = tt.typeName,
					@atype2 = tt.TypeName2
			from (
				select 	
					e.EntityName,
					a.TypeName,
					row_number() over (order by a.attributeID) AS rowNum,
					isnull(a.transformStr, a.attributeName) as attributeName,
					a.isPK,
					a.isAddPK,
					a.is_slidingDate,
					a.is_slidingDate2,
					a.refEntityName,
					isnull(eref.alias,eref.entityName) as refAlias,
					a.refAttributeName as refAttributeName,
					aref.attributeName as refSlidingDate,
					convert(nchar(8),dateadd(day,-mcd.DeltaDays,mcd.MaxChangeDate),112) as MaxChangeDate,
					mcd.MaxChangeCode as MaxChangeCode,
					aref.typeName as TypeName2
				from dbo.attribute_STG a
					inner join dbo.entity_STG e on a.entityID = e.entityID
					left outer join dbo.entity_STG eref on a.refEntityName = eref.EntityName and a.schemaName = eref.schemaName
					left outer join dbo.attribute_STG aref on eref.entityID = aref.entityID and aref.is_slidingDate = 1
					left outer join dbo.max_changeDate mcd on a.attributeDWHID = mcd.attributeID
				where a.isUse = 1 and a.entityName = @entityName and a.schemaName = @schemaName
			) tt where tt.rowNum = @rowStep

		-- определим строку для каждого столбца
		if (@alias is not null)
			set @attributeName =  @alias + '.' + @attributeName
		--AG
		--При скользящем окне типа timestamp нужно преобразовать атрибут в строку или дату, иначе при сравнении сравниваются длины строк, а не даты
		If (@atype = 'TimeStamp' and @schemaName in ('sap','crm'))
			SET @isTimeStamp = 1
		ELSE 
			SET @isTimeStamp = 0;

		if (@isTimeStamp = 1)
			set @attributeName = 'to_char('+ @attributeName+ ') '		
		
		if (@isFullDelete = 0 or @isPK = 1 or @isAddPK = 1 or @typeLoad in ('incremental','everyHour'))
		begin
			set @selectStr = @selectStr + @attributeName
			
			if (@isTimeStamp = 1) 
				set @selectStr = @selectStr + 'as '+Replace(Replace(Replace(@attributeName,'to_char(',''),')',''),@alias+'.','')
		end

		if ((@refSlidingDate is not null or @is_slidingDate = 1) and (@isCRM = 0 and (@typeLoad in ('incremental','everyHour') or @isFact = 1) 
			or (@isCRM = 1 and @typeLoad not in ('incremental','everyHour'))))
		begin
			if (@refEntityName is not null)
				exec dbo.get_SlidingDate @schemaName, @refEntityName, @typeLoad, @SlidingDateParam output
			ELSE 
				EXEC dbo.get_SlidingDate @schemaName, @entityName, @typeLoad, @SlidingDateParam OUTPUT

			IF (@isPOS = 0 AND @isPOSSet = 0 AND @isPOSLoy = 0 AND @isFact = 1 AND @isCRM = 0)			-- не POS и факты из САПа - грузятся через dataFow								
				set @SlidingDateParam = '''' + @SlidingDateParam + ''''

			ELSE IF (@isCRM = 1 AND @isFact = 1)
				set @SlidingDateParam = @SlidingDateParam + '000000'

			ELSE
				SET @SlidingDateParam = '''''' + @SlidingDateParam + ''''''
		END
		
		ELSE IF (@MaxChangeDate IS NOT NULL)
		begin
			if (@isFact = 1)
				set @SlidingDateParam = '''' + @MaxChangeDate + ''''
			ELSE
				SET @SlidingDateParam = '''''' + @MaxChangeDate + ''''''
		END

		ELSE IF (@MaxChangeCode IS NOT NULL)
		BEGIN
			IF (@isFact = 1)
				set @SlidingDateParam = @MaxChangeCode
			ELSE
				SET @SlidingDateParam = '''' + @MaxChangeCode + ''''
		END

		if (@isUseSlidingDate = 0 and @typeLoad not in ('incremental','everyHour') and @SlidingDateParam is not null and @refSlidingDate is null and @MaxChangeCode is null)
			set @SlidingDateParam = null

		if (@refEntityName is not null and @refAlias is not null and (@typeLoad in ('incremental','everyHour') or @isFact = 1))
		begin
			if (len(@existsClause) = 0)
				set @existsClause = @existsClause + 'exists (select 1 from ' + @sourceSchema + @refEntityName + ' ' + @refAlias + ' where '
			ELSE
				SET @existsClause = @existsClause + ' and '
					
			set @existsClause = @existsClause + @attributeName + '=' + @refAlias + '.' + @refAttributeName

			IF (@refSlidingDate IS NOT NULL)
				SET @slidingClause = @refAlias + '.' + @refSlidingDate + '>=' + @SlidingDateParam
		END
		
		ELSE IF (@SlidingDateParam IS NOT NULL AND (@is_slidingDate = 1 OR @is_slidingDate2 = 1 OR ((@MaxChangeDate IS NOT NULL OR @MaxChangeCode IS NOT NULL) AND @typeLoad IN ('incremental','everyHour'))))
		BEGIN
			IF (LEN(@slidingClause) = 0 AND @is_slidingDate2 = 0)
			begin
				set @slidingClause = @attributeName + '>=' + @SlidingDateParam

				IF (@isPOS = 0 AND @isPOSSet = 0 AND @isPOSLoy = 0 AND @isFact = 1 AND @isCRM = 0)
					set @AddSlidingClause = @attributeName + '=''00000000'' and '
				ELSE
					SET @AddSlidingClause = @attributeName + '=''''00000000'''' and '
			END

			ELSE IF (@is_slidingDate2 = 1)
				set @slidingClause = '(' + @slidingClause + ' or ' + @AddSlidingClause + @attributeName + '>=' + @SlidingDateParam + ')'
			ELSE
				SET @slidingClause = '(' + @slidingClause + ' or ' + @attributeName + '>=' + @SlidingDateParam + ')'
		END
	
		if (@rowStep < @rowCount 
				and (@isFullDelete = 0 or @isPK = 1 or @isAddPK = 1 or @typeLoad in ('incremental','everyHour')))
			set @selectStr = @selectStr + ','

		SET @rowStep = @rowStep + 1	
	END

	if (right(@selectStr,1) = ',')
		set @selectStr = left(@selectStr,len(@selectStr)-1)

	if (@alias is not null)
		set @entityName = @entityName + ' ' + @alias

	set @selectStr = @selectStr + ' from ' + @sourceSchema + @entityName --+ iif(@schemaName = 'axp',' (nolock) ','')

	IF (@isUse = 0 
		OR @typeLoad NOT IN ('incremental','nightFull','weekFull','everyHour')
		OR @typeLoad = 'incremental' AND @isIncremental = 0
		OR @typeLoad = 'nightFull' AND @isNightFull = 0
		OR @typeLoad = 'weekFull' AND @isWeekFull = 0
		OR @typeLoad = 'everyHour' AND @isEveryHour = 0)
	begin
		set @whereClause = @whereClause + '1<>1'
	end

	ELSE

	BEGIN
		IF (@schemaName IN ('sap','crm'))
		BEGIN
			IF (LEN(@clientField) > 0)
			BEGIN
				set @whereClause = @whereClause + isnull(@alias + '.','') + @clientField + ' = '
				IF (@schemaName = 'sap')
				begin
					if (@isFact = 1)
						set @whereClause = @whereClause + '''600'''
					ELSE
						SET @whereClause = @whereClause + '''''600'''''
				END	
				ELSE IF (@schemaName = 'crm')
				BEGIN
					IF (@isFact = 1)
						set @whereClause = @whereClause + '''900'''
					ELSE
						SET @whereClause = @whereClause + '''''900'''''
				END	
			END
		END
	
		IF (LEN(@slidingClause) > 0)
		BEGIN
			IF (LEN(@existsClause) = 0)
				SET @existsClause = @slidingClause
			ELSE	
			BEGIN
				SET @existsClause = @existsClause + ' and ' + @slidingClause
				SET @existsClause = @existsClause + ')'
			END
		END

		IF (LEN(@existsClause) > 0 AND @whereClause = ' where ')
			SET @whereClause = @whereClause + @existsClause

		ELSE IF (LEN(@existsClause) > 0)
			SET @whereClause = @whereClause + ' and ' + @existsClause

		IF (@typeLoad IN ('incremental','everyHour') OR @isAddQueryStrIncremental = 0)
			SET @whereClause = @whereClause + @addQueryStr
	END

	IF (@whereClause = ' where ')
		SET @whereClause = ''

	SET @selectStr = @selectStr + @whereClause

--	if (@typeLoad in ('incremental','everyHour') or @isAddQueryStrIncremental = 0)
--		set @selectStr = @selectStr + @addQueryStr


	IF (@schemaName IN ('sap','crm'))
		SET @selectStr = @selectStr + ' with ur'

	UPDATE dbo.entity_STG
		SET selectStr = @selectStr
		WHERE schemaName = @schemaName AND entityName = @entityName

	SELECT @selectStr AS selectFromSource
		
	SET @rowStepUp = @rowStepUp + 1	
END

-- exec dbo.get_selectFromSource 'sap','fmglflext'

GO
