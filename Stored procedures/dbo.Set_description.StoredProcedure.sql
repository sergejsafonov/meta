USE [meta]
GO
/****** Object:  StoredProcedure [dbo].[Set_description]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Set_description]
	@dbName nvarchar(32) ='dwh',
	@schemaName nvarchar(128) = null,
	@entityName nvarchar(128) = null,
	@attributeName nvarchar(256) = null,
	@caption nvarchar(64) = null,
	@description nvarchar(256) = null
AS

declare @rowCount int, @rowStep int, @captionColumn nvarchar(64), @captionDescription nvarchar(256), @use nvarchar(32)
declare @setProperty nvarchar(4000), @setTableProperty nvarchar(4000), @setColumnProperty nvarchar(4000), @ProcedureName nvarchar(128)
declare @is_ID bit, @relAttributeID int, @attributeRealName nvarchar(256), @relEntityName nvarchar(128), @relSchemaName nvarchar(128)

if (@schemaName is null and @attributeName is null)
begin
	raiserror('Не указана @schemaName!',16,1)
	return
end
if (@entityName is null and @attributeName is null)
begin
	raiserror('Не указана @entityName!',16,1)
	return
end
	
if (@attributeName is null)								-- устанавливаем описания на уровне таблицы
begin
	if (@caption is null)
	begin
		select 
			 @caption = e.caption
		from meta.dbo.entity_DWH e
		where e.isUse = 1 and e.entityName = @entityName and e.schemaName = @schemaName

		if (@caption is null)
		begin
			raiserror('Не указан caption!',16,1)
			return
		end
	end
	else
		update meta.dbo.entity_DWH
			set caption = @caption
			where isUse = 1 and entityName = @entityName and schemaName = @schemaName

	if (@description is null)
	begin
		select 
			 @description = e.description
		from meta.dbo.entity_DWH e
		where e.isUse = 1 and e.entityName = @entityName and e.schemaName = @schemaName
	end
	else
		update meta.dbo.entity_DWH
			set description = @description
			where isUse = 1 and entityName = @entityName and schemaName = @schemaName

	if (exists (SELECT 1 FROM fn_listextendedproperty ('Caption', 'Schema', @schemaName, 'Table', @entityName, default, default)))						
		set @ProcedureName = 'sp_updateextendedproperty'			
	else
		set @ProcedureName = 'sp_addextendedproperty'
						
	set @setProperty = @ProcedureName + ' @name = N''Caption'',@value = N''' + @caption + '''' 

	set @setTableProperty = ', @level0type = N''Schema'',@level0name = N''' + @schemaName + ''''
	set @setTableProperty = @setTableProperty + ', @level1type = N''Table'',@level1name = N''' + @entityName + ''''

	set @setProperty = 	@setProperty + @setTableProperty + ';'

--	select @setProperty
	exec (@setProperty)

	if (@description is not null)
	begin
		if (exists (SELECT 1 FROM fn_listextendedproperty ('Description', 'Schema', @schemaName, 'Table', @entityName, default, default)))			
			set @ProcedureName = 'sp_updateextendedproperty'			
		else
			set @ProcedureName = 'sp_addextendedproperty'

		set @setProperty = 	@ProcedureName + ' @name = N''Description'',@value = N''' + @description + '''' 
		set @setProperty = 	@setProperty + @setTableProperty + ';'
	
--		select @setProperty
		exec (@setProperty)
	end

	select @rowCount = count(*) 
		from meta.dbo.entity_DWH e
			inner join meta.dbo.attribute_DWH a on e.entityID = a.entityID
		where e.isUse = 1 and a.isUse = 1 
			and e.entityName = @entityName

	set @rowStep = 1
	while (@rowStep <= @rowCount)
	begin
		select
			 @entityName = tt.entityName
			,@schemaName = tt.schemaName
			,@attributeRealName = tt.attributeName
		from (select row_number() over (order by e.entityID) AS rowNum
				,e.entityName
				,e.schemaName
				,a.attributeName
			from meta.dbo.entity_DWH e
				inner join meta.dbo.attribute_DWH a on e.entityID = a.entityID
			where e.isUse = 1 and a.isUse = 1 
				and e.entityName = @entityName
			) tt where tt.rowNum = @rowStep

		select 
			 @caption = a.caption,
			 @description = a.description
		from meta.dbo.attribute_DWH a
		where a.isUse = 1 and a.entityName = @entityName and a.schemaName = @schemaName and a.attributeName = @attributeRealName		

		if (@caption is not null)
		begin
			if (exists (SELECT 1 FROM fn_listextendedproperty ('Caption', 'Schema', @schemaName, 'Table', @entityName, 'Column', @attributeRealName)))			
				set @ProcedureName = 'sp_updateextendedproperty'			
			else
				set @ProcedureName = 'sp_addextendedproperty'

			set @setProperty = @ProcedureName + ' @name = N''Caption'',@value = N''' + @caption + '''' 

			set @setTableProperty = ', @level0type = N''Schema'',@level0name = N''' + @schemaName + ''''
			set @setTableProperty = @setTableProperty + ', @level1type = N''Table'',@level1name = N''' + @entityName + ''''
			set @setColumnProperty = ', @level2type = N''Column'',@level2name = N''' + @attributeRealName + ''';'

			set @setProperty = 	@setProperty + @setTableProperty + @setColumnProperty

	--		select @setProperty
			exec (@setProperty)
		end

		if (@description is not null)
		begin
			if (exists (SELECT 1 FROM fn_listextendedproperty ('Description', 'Schema', @schemaName, 'Table', @entityName, 'Column', @attributeRealName)))	
				set @ProcedureName = 'sp_updateextendedproperty'			
			else
				set @ProcedureName = 'sp_addextendedproperty'

			set @setProperty = 	@ProcedureName + ' @name = N''Description'',@value = N''' + @description + ''''
			set @setProperty = 	@setProperty + @setTableProperty + @setColumnProperty

--			select @setProperty
			exec (@setProperty)
		end

		set @caption = null
		set @rowStep = @rowStep + 1	
	end		
end
else													-- устанавливаем описания на уровне столбца
begin
	if (@entityName is not null and @schemaName is not null)
		set @rowCount = 1
	else
		select @rowCount = count(*) 
			from meta.dbo.entity_DWH e
				inner join meta.dbo.attribute_DWH a on e.entityID = a.entityID
			where e.isUse = 1 and a.isUse = 1 
				and (a.attributeName = @attributeName
						or @attributeName = 'PK' and a.is_ID = 1 
						or @attributeName = 'FK' and a.relAttributeID is not null and a.is_ID = 0)

	set @rowStep = 1
	while (@rowStep <= @rowCount)
	begin
		if (@entityName is null or @schemaName is null)
			select
				 @entityName = tt.entityName
				,@schemaName = tt.schemaName
				,@attributeRealName = tt.attributeName
				,@is_ID = tt.is_ID
				,@relAttributeID = tt.relAttributeID
				,@relEntityName = tt.relEntityName
				,@relSchemaName = tt.relSchemaName
			from (select row_number() over (order by e.entityID) AS rowNum
					,e.entityName
					,e.schemaName
					,a.attributeName
					,a.is_ID
					,a.relAttributeID
					,a1.entityName as relEntityName
					,a1.schemaName as relSchemaName
				from meta.dbo.entity_DWH e
					inner join meta.dbo.attribute_DWH a on e.entityID = a.entityID
					left join meta.dbo.attribute_DWH a1 on a.relAttributeID = a1.attributeID
				where e.isUse = 1 and a.isUse = 1 
					and (a.attributeName = @attributeName
						or @attributeName = 'PK' and a.is_ID = 1 
						or @attributeName = 'FK' and a.relAttributeID is not null and a.is_ID = 0)
			) tt where tt.rowNum = @rowStep

		else
			set @attributeRealName = @attributeName

		if (@caption is null and @attributeName = 'PK' and @is_ID = 1)
		begin
			set @caption = 'Первичный ключ'
			set @description = 'Уникальный суррогатный идентификатор'
		end

		if (@caption is null and @attributeName = 'FK' and @relAttributeID is not null and @is_ID = 0)
		begin
			set @caption = 'Внешний ключ'
			set @description = 'Внешний ключ к таблице ' + @relSchemaName + '.' + @relEntityName
		end

		if (@caption is null and @attributeName = 'Code')
		begin
			set @caption = 'Код'
			set @description = 'Уникальный натуральный идентификатор'
		end

		if (@caption is null and @attributeName = 'Composite')
		begin
			set @caption = 'Композитный код'
			set @description = 'Уникальный композитный бинарный код'
		end

		if (@caption is null and @attributeName = 'FlagOperation')
		begin
			set @caption = 'Флаг загрузки в dwh'
			set @description = 'Признак загрузки строки: 1-новая, 2-изменённая,3-удалённая'
		end

		if (@caption is null and @attributeName = 'DateOperation')
			set @caption = 'Дата-время загрузки в dwh'

		if (@caption is null and @attributeName = 'CreationDatetime')
			set @caption = 'Дата-время создания'
					
		if (@caption is null and @attributeName = 'CreationDate')
			set @caption = 'Дата создания'

		if (@caption is null and @attributeName = 'CreationTime')
			set @caption = 'Время создания'
		
		if (@caption is null and @attributeName = 'ChangeDate')
		begin
			set @caption = 'Дата изменения'
			set @description = 'Используется при обновлении данных'
		end

		if (@caption is null and @attributeName = 'ChangeDateTimeID')
			set @caption = 'Дата-время изменения'
		
		if (@caption is null and @attributeName = 'DateExpiration')
		begin
			set @caption = 'Дата-время конца периода действия'
			set @description = 'Совместно с DateOperation задаёт период действия строки данных (для SCD 2)'
		end
		
		if (@caption is null and @attributeName in ('isCurrent','FlagCurrent'))
		begin
			set @caption = 'Признак актуальности'
			set @description = 'Признак, что строка актуальна на текущий момент'
		end
		
		if (@caption is null and @attributeName = 'FlagOld')
		begin
			set @caption = 'Признак старого POS'
			set @description = 'Признак данных, загружаемых со старой бд POS'
		end
		
		if (@caption is null and @attributeName = 'DBsourceID')
		begin
			set @caption = 'ИД БД POS'
			set @description = 'Идентификатор бд POS: 1 - Розница, 2 - БУ'
		end
		
		if (@caption is null and @attributeName = 'DeliveryCode')
		begin
			set @caption = 'Код документа поставки'
			set @description = '= Delivery.DeliveryCode'
		end
		
		if (@caption is null and @attributeName = 'DeliveryPositionCode')
		begin
			set @caption = 'Код позиции документа поставки'
			set @description = '= Delivery.DeliveryPositionCode'
		end
		
		if (@caption is null and @attributeName = 'PurchasingCode')
		begin
			set @caption = 'Код заказа'
			set @description = '= Purchasing.PurchasingCode'
		end
		
		if (@caption is null and @attributeName = 'PurchaseCode')
		begin
			set @caption = 'Код чека'
			set @description = '= pos.Purchase.PurchaseCode'
		end
		
		if (@caption is null and @attributeName = 'PositionCode')
			set @caption = 'Код позиции документа'

		
		if (@caption is null and @attributeName = 'WarehouseNumber')
		begin
			set @caption = 'Код склада учёта'
			set @description = 'SAP: T300-LGNUM'
		end
	
		if (@caption is null)
			select 
				 @caption = a.caption
			from meta.dbo.attribute_DWH a
			where a.isUse = 1 and a.entityName = @entityName and a.schemaName = @schemaName and a.attributeName = @attributeRealName
		else
			update meta.dbo.attribute_DWH
				set caption = @caption
				where isUse = 1 and entityName = @entityName and schemaName = @schemaName and attributeName = @attributeRealName

		if (@description is null)
			select 
				 @description = a.description
			from meta.dbo.attribute_DWH a
			where a.isUse = 1 and a.entityName = @entityName and a.schemaName = @schemaName and a.attributeName = @attributeRealName
		else
			update meta.dbo.attribute_DWH
				set description = @description
				where isUse = 1 and entityName = @entityName and schemaName = @schemaName and attributeName = @attributeRealName
						
		if (@caption is null)
		begin
			raiserror('Не указан caption!',16,1)
			return
		end		

		set @use = 'use ' + @dbName
		exec (@use)

		if (exists (SELECT 1 FROM fn_listextendedproperty ('Caption', 'Schema', @schemaName, 'Table', @entityName, 'Column', @attributeRealName)))			
			set @ProcedureName = 'sp_updateextendedproperty'			
		else
			set @ProcedureName = 'sp_addextendedproperty'

		set @setProperty = @ProcedureName + ' @name = N''Caption'',@value = N''' + @caption + '''' 

		set @setTableProperty = ', @level0type = N''Schema'',@level0name = N''' + @schemaName + ''''
		set @setTableProperty = @setTableProperty + ', @level1type = N''Table'',@level1name = N''' + @entityName + ''''
		set @setColumnProperty = ', @level2type = N''Column'',@level2name = N''' + @attributeRealName + ''';'

		set @setProperty = 	@setProperty + @setTableProperty + @setColumnProperty

		select @setProperty
		exec (@setProperty)

		if (@description is not null)
		begin
			if (exists (SELECT 1 FROM fn_listextendedproperty ('Description', 'Schema', @schemaName, 'Table', @entityName, 'Column', @attributeRealName)))	
				set @ProcedureName = 'sp_updateextendedproperty'			
			else
				set @ProcedureName = 'sp_addextendedproperty'

			set @setProperty = 	@ProcedureName + ' @name = N''Description'',@value = N''' + @description + ''''
			set @setProperty = 	@setProperty + @setTableProperty + @setColumnProperty

			select @setProperty
			exec (@setProperty)
		end

		set @entityName = null
		set @schemaName = null
		set @caption = null
		
		set @rowStep = @rowStep + 1	
	end		
end

return


GO
