USE [meta]
GO

alter PROCEDURE [dbo].[sync_entity_STG]
AS

update e
	set [object_id] = so.[object_id]
	from dbo.entity_STG e
		inner join [STG].[sys].[schemas] sc on e.schemaName = sc.name
		inner join [STG].[sys].[objects] so on e.entityName = so.name and sc.[schema_id] = so.[schema_id]
	where e.[object_id] <> so.[object_id] or e.[object_id] is null

update e
	set entityName = so.name
	from dbo.entity_STG e
		inner join STG.[sys].[objects] so on e.[object_id] = so.[object_id]
		inner join STG.[sys].[schemas] sc on so.schema_id = sc.schema_id and sc.[name] = e.schemaName
	where e.entityName <> so.name 

delete from a
	from dbo.attribute_STG a
		inner join dbo.entity_STG e on a.entityID = e.entityID and a.schemaName = e.schemaName
	where not exists (select 1 from STG.[sys].[columns] sc 
						where sc.[object_id] = e.[object_id] and sc.name = a.attributeName)

delete from e
	from dbo.entity_STG e
	where not exists (select 1 from [stg].[sys].[objects] so 
							inner join [STG].[sys].[schemas] sc on so.schema_id = sc.schema_id 
						where e.[object_id] = so.[object_id] or e.[object_id] is null)
		and not exists (select 1 from dbo.attribute_STG a where a.entityID = e.entityID)

delete from e
	from dbo.entity_STG e
	where not exists (select 1 from [stg].[sys].[objects] so 
							inner join [STG].[sys].[schemas] sc on so.schema_id = sc.schema_id 
						where e.entityName = so.name and e.schemaName = sc.name)	
		and not exists (select 1 from dbo.attribute_STG a where a.entityID = e.entityID)

insert into dbo.entity_STG (schemaName, entityName, [object_id], isUse)
	SELECT sch.[name] as schemaName
		,so.name as entityName
		,so.[object_id]
		,0 as isUse
    FROM [STG].[sys].[objects] so
		inner join [STG].[sys].[schemas] sch on so.[schema_id] = sch.[schema_id]
	where so.type = 'U' 
		and not exists (select 1 from meta.dbo.entity_STG e where so.[object_id] = e.[object_id])
	order by entityName

insert into dbo.attribute_STG (entityID,entityName,schemaName,attributeName,columnID,typeName,is_nullable,is_ID)
	SELECT 
	   e.entityID
	  ,e.entityName
	  ,e.schemaName
      ,sc.name as attributeName
      ,sc.column_id as columnID
	  ,(case when st.name = 'nvarchar' then st.name + '(' + cast(sc.[max_length] as nvarchar(4)) + ')' 
			when st.name = 'varchar' then iif(e.schemaName = 'axp','','n') + st.name + '(' + cast(sc.[max_length] as nvarchar(4)) + ')' 
			when st.name in ('decimal','numeric') and sc.name not in ('POSTING_DATE','CHANGED_AT','CREATED_AT') then st.name + '(15,3)' 
			when sc.name in ('POSTING_DATE','CHANGED_AT','CREATED_AT') then st.name + '(15,0)' 
			when st.name = 'binary' then st.name + '(' + cast(sc.[max_length] as nvarchar(4)) + ')' 
			else st.name end) as typeName
      ,sc.[is_nullable]
      ,sc.[is_identity] as isID
	FROM [STG].[sys].[columns] sc
		inner join dbo.entity_STG e on sc.[object_id] = e.[object_id]
		inner join [STG].[sys].[types] st on sc.[system_type_id] = st.[system_type_id]
	where st.name <> 'sysname' and sc.name <> 'UID'
		and not exists (select 1 from meta.dbo.attribute_STG a where e.entityName = a.entityName and e.schemaName = a.SchemaName and sc.name = a.attributeName)
	order by entityName,columnID;

with ATTRIBUTE_STG_column
as (
	select
		attributeID,
		row_number() over (partition by entityName,schemaName order by attributeID) columnID
	from DBO.ATTRIBUTE_STG
	)
update a
	set columnID = ac.columnID
	from DBO.ATTRIBUTE_STG a
		inner join ATTRIBUTE_STG_column ac on a.attributeID = ac.attributeID
	where a.columnID <> ac.columnID

update a
	set is_slidingDate = 1
	from dbo.attribute_STG a 
		inner join dbo.entity_STG e on a.schemaName = e.SchemaName and a.entityName = e.EntityName and e.isUse = 1
		inner join dbo.SlidingDate sd on a.schemaName = sd.SchemaName and a.entityName = sd.EntityName and a.attributeName = sd.AttributeName
	where a.is_slidingDate = 0
update a
	set is_slidingDate = 0
	from dbo.attribute_STG a 
		inner join dbo.entity_STG e on a.schemaName = e.SchemaName and a.entityName = e.EntityName and e.isUse = 1
	where a.is_slidingDate = 1
		and not exists (select 1 from dbo.SlidingDate sd where a.schemaName = sd.SchemaName and a.entityName = sd.EntityName and a.attributeName = sd.AttributeName)

update e
	set isUseSlidingDate = 1
	from dbo.entity_STG e
	where exists (select 1 from dbo.attribute_STG a where e.entityID = a.entityID and a.is_slidingDate = 1)
		and e.isUseSlidingDate = 0 and e.isUse = 1
update e
	set isUseSlidingDate = 0
	from dbo.entity_STG e
	where not exists (select 1 from dbo.attribute_STG a where e.entityID = a.entityID and a.is_slidingDate = 1)
		and isUseSlidingDate = 1 and isUse = 1

update dbo.entity_STG
	set isUseSlidingDate = 0
	where entityName in ('ZDEVFI49_USLSUDY')
		and isUseSlidingDate = 1

update sd
	set SlidingValue = null
	from dbo.SlidingDate sd
		inner join dbo.entity_stg e on sd.SchemaName = e.schemaName and sd.EntityName = e.entityName and e.isIncremental = 0
	where sd.SlidingValue is not null 
update sd
	set SlidingValueNight = null
	from dbo.SlidingDate sd
		inner join dbo.entity_stg e on sd.SchemaName = e.schemaName and sd.EntityName = e.entityName and e.isNightFull = 0
	where sd.SlidingValueNight is not null
update sd
	set SlidingValueWeek = null
	from dbo.SlidingDate sd
		inner join dbo.entity_stg e on sd.SchemaName = e.schemaName and sd.EntityName = e.entityName and e.isWeekFull = 0
	where sd.SlidingValueWeek is not null

return

-- exec dbo.sync_entity_STG
GO
