USE [meta]
GO
/****** Object:  StoredProcedure [dbo].[Create_DWH_tables]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Create_DWH_tables]
	 @isOnlyDrop bit =0
	,@schemaName nvarchar(128) = null
	,@entityName nvarchar(128) = null
AS

declare @rowCountUp int, @rowStepUp int, @rowCount int, @rowStep int, @createFKTable nvarchar(max), @is_ID bit, @collation nvarchar(128)
declare @dropTable nvarchar(max), @createTable nvarchar(max), @entityID int, @PKattributeName nvarchar(128), @createPKStr nvarchar(max), @isService bit
declare @attributeName nvarchar(128), @typeName nvarchar(32), @is_nullable bit, @defaultValue nvarchar(256), @isColumnStore bit, @isDim bit, @isFact bit
declare @relEntityName nvarchar(128), @relSchemaName nvarchar(128), @relAttributeName nvarchar(128), @createCsIndex nvarchar(max), @isRelation bit

if (@schemaName is null or @schemaName not in ('sap','pos','dbo','xls'))
begin
	raiserror('Не указана @schemaName!',16,1)
	return
end
if (@entityName is null)
begin
	raiserror('Не указана @entityName!',16,1)
	return
end

-- определим количество иттераций по создаваемым таблицам для цикла
if (@entityName is not null)
	select @rowCountUp = 1
else
	select @rowCountUp = count(*) 
		from dbo.entity_DWH e
		where e.isUse = 1

set @createFKTable  = ''

set @rowStepUp = 1
while (@rowStepUp <= @rowCountUp)
begin
	if (@rowStepUp = 1 and @entityName is not null)
		select 
				 @entityID = e.entityID
				,@isColumnStore = e.isColumnStore
				,@isDim = e.isDim
				,@isFact = e.isFact
				,@isRelation = e.isRelation
				,@isService = e.isService
			from dbo.entity_DWH e
			where e.entityName = @entityName and e.schemaName = @schemaName and e.isUse = 1
	else
		select
			 @entityID = tt.entityID 
			,@entityName = tt.entityName
			,@schemaName = tt.schemaName
			,@isColumnStore = tt.isColumnStore
			,@isDim = tt.isDim
			,@isFact = tt.isFact
			,@isRelation = tt.isRelation
			,@isService = tt.isService
		from (select row_number() over (order by e.sortOrder desc, e.entityID) AS rowNum
					,e.entityID
					,e.entityName
					,e.schemaName
					,e.isColumnStore
					,e.isDim
					,e.isFact
					,e.isRelation
					,e.isService
				from dbo.entity_DWH e
				where e.isUse = 1 
			) tt where tt.rowNum = @rowStepUp
		
	set @dropTable = 'if object_id(''DWH.' + @schemaName + '.' + @entityName + 
		''') is not null drop table DWH.' + @schemaName + '.' + @entityName

	set @createTable = 'create table DWH.' + @schemaName + '.' + @entityName + ' ('

	if (@isColumnStore = 0)	
		set @createPKStr = ',constraint PK_' + @entityName + ' primary key clustered ('

	select @rowCount = count(*) 
		from dbo.attribute_DWH  a
				where a.isUse = 1 and a.entityID = @entityID

	set @rowStep = 1
	-- цикл определения признаков столбцов для создания таблицы
	while (@rowStep <= @rowCount)
	begin
		select 
				 @attributeName = tt.attributeName
				,@typeName = tt.typeName
				,@is_ID = tt.is_ID
				,@is_nullable = tt.is_nullable
				,@defaultValue = tt.defaultValue
				,@relSchemaName = tt.relSchemaName
				,@relEntityName = tt.relEntityName
				,@relAttributeName = tt.relAttributeName
				,@collation = tt.collation
			from (
				select row_number() over (order by a.columnID) AS rowNum,
					a.attributeName, 
					a.typeName, 
					a.is_ID,
					a.is_nullable, 
					a.defaultValue,
					e1.schemaName as relSchemaName,
					e1.entityName as relEntityName,
					a1.attributeName as relAttributeName,
					isnull('COLLATE ' + a.collation,'') collation
				from dbo.attribute_DWH a
					left outer join dbo.attribute_DWH a1 on a.relAttributeID = a1.AttributeID
					left outer join dbo.entity_DWH e1 on a1.entityID = e1.entityID
				where a.isUse = 1 and a.entityName = @entityName
			) tt where tt.rowNum = @rowStep

		-- определим строку для каждого столбца
		set @createTable = @createTable + @attributeName + ' ' + @typeName + ' ' + @collation + ' '
		set @createTable = @createTable + (case @is_nullable when 1 then '' else 'not ' end) + 'null'
		set @createTable = @createTable + (case when @defaultValue is not null then ' default(' + @defaultValue + ')' else '' end)

		if (@is_ID = 1 and @isService = 0)
			set @createTable = @createTable + ' identity(' + (case when @isFact = 1 or @isRelation = 1 then '1' else '0' end) + ',1)'

		if (@isColumnStore = 0 and @is_ID = 1)	
			set @createPKStr = @createPKStr + @attributeName + ','

		if (@relAttributeName is not null and @isColumnStore = 0)
			set @createFKTable = @createFKTable + 'alter TABLE DWH.' + @schemaName + '.' + @entityName + ' add constraint FK_' + @entityName + '_' + @attributeName + 
				' foreign key (' + @attributeName + ') references ' + @relSchemaName + '.' + @relEntityName + ' (' + @relAttributeName + ') '

		if (@rowStep < @rowCount)
			set @createTable = @createTable + ','
		
		set @rowStep = @rowStep + 1	
	end

	if (@isColumnStore = 0)	
		set @createTable = @createTable + left(@createPKStr,len(@createPKStr)-1) + ')'

	set @createTable = @createTable + ') ON [PRIMARY]'
	
	select @dropTable
	exec (@dropTable)	
	
	if (@isOnlyDrop = 0)
	begin
		-- скрипт создания таблицы
		select @createTable
		exec (@createTable)

		if (@isColumnStore = 1)
		begin
			set @createCsIndex = 'CREATE CLUSTERED COLUMNSTORE INDEX ccl_' + @entityName + ' ON DWH.' + @schemaName + '.' + @entityName

			select @createCsIndex
			exec (@createCsIndex)
		end
	end

	set @rowStepUp = @rowStepUp + 1	
end

if (@isOnlyDrop = 0 and @createFKTable <> '')
begin
	select @createFKTable
	exec (@createFKTable)
end


GO
