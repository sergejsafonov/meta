USE [meta]
GO
/****** Object:  StoredProcedure [dbo].[BulkLoadFileToStg]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[BulkLoadFileToStg]
	@typeLoad nvarchar(32) = 'incremental',
	@isGetReal bit = null,
	@Name nvarchar(32) = null
as

declare @rowStep int, @rowCount int, @BulkInsertStr nvarchar(max), @resultEntity nvarchar(512), @localHost nvarchar(max), @codepage nvarchar(32)
declare @datefiletype nvarchar(32), @isKeepNulls bit, @FirstRow nvarchar(8), @FieldTerminator nvarchar(8), @RowTerminator nvarchar(8)
declare @deleteFile nvarchar(512), @fileExistsFile nvarchar(512), @isIncremental bit, @isNightFull bit  

if (@typeLoad = 'incremental')
begin
	set @isIncremental = 1
	set @isNightFull = 0
end
else
begin
	set @isIncremental = 0
	set @isNightFull = 1
end

if (@Name is not null)
	set @rowCount = 1
else	
	select @rowCount = count(*)
		from dbo.fileTransfer ft
		where ft.isXLS = 0 
			and ft.isReverse = 0 
			and (ft.isGetReal = @isGetReal and ft.isExternalSource = 0 or @isGetReal is null and (ft.isGetReal = 0 or ft.isExternalSource = 1))
			and ft.isIncremental = @isIncremental 
			and ft.isNightFull = @isNightFull
			and ft.isUse = 1

set @rowStep = 1
while (@rowStep <= @rowCount)
begin
	select 
		@Name = tt.Name,
		@resultEntity = tt.resultEntity , 
		@localHost = tt.localHost, 
		@codepage = tt.codepage,
		@datefiletype = tt.datefiletype, 
		@isKeepNulls = tt.isKeepNulls, 
		@FirstRow = tt.FirstRow, 
		@FieldTerminator = tt.FieldTerminator, 
		@RowTerminator = tt.RowTerminator	
	from (select row_number() over (order by ft.fileTransferID) AS rowNum,
			ft.Name,
			ft.resultEntity , 
			ft.localHost , 
			ft.codepage ,
			ft.datefiletype , 
			ft.isKeepNulls , 
			cast(ft.FirstRow as nvarchar(8)) FirstRow, 
			ft.FieldTerminator , 
			ft.RowTerminator	
		from dbo.fileTransfer ft
		where ft.isXLS = 0 
			and ft.isReverse = 0 
			and (ft.Name = @Name or @Name is null)
			and (ft.isGetReal = @isGetReal and ft.isExternalSource = 0 or @isGetReal is null and (ft.isGetReal = 0 or ft.isExternalSource = 1))
			and ft.isIncremental = @isIncremental 
			and ft.isNightFull = @isNightFull
			and ft.isUse = 1
		) tt where tt.rowNum = @rowStep

	begin try
		set @fileExistsFile = 'declare @isExists int exec master.dbo.xp_fileexist ''' + @localHost
		set @fileExistsFile = @fileExistsFile + ''', @isExists OUTPUT if (@isExists = 1) select @isExists'

	--	select @fileExistsFile
		exec (@fileExistsFile)

		if (@@rowcount > 0)
		begin
			set @BulkInsertStr = 'BULK INSERT ' + @resultEntity + ' FROM ''' + @localHost + ''' WITH ( ' 
			set @BulkInsertStr = @BulkInsertStr + 'CODEPAGE=''' + @codepage + ''',DATAFILETYPE=''' + @datefiletype
			set @BulkInsertStr = @BulkInsertStr + ''',' + (case @isKeepNulls when 1 then 'KEEPNULLS' else '' end)
			set @BulkInsertStr = @BulkInsertStr + ',FIRSTROW=' + @FirstRow
			set @BulkInsertStr = @BulkInsertStr + iif(@FieldTerminator is not null,',FIELDTERMINATOR=''' + @FieldTerminator + '''','')
			set @BulkInsertStr = @BulkInsertStr + iif(@RowTerminator is not null,',ROWTERMINATOR=''' + @RowTerminator +'''','') + ');'
			
			select @BulkInsertStr
			exec (@BulkInsertStr)
		/*
			set @deleteFile = 'master..xp_cmdshell ''del ' + @localHost + ''''

			select @deleteFile
		--	exec (@deleteFile)
		*/
			update dbo.fileTransfer
				set DateOperation = getdate(),
					LoadCount = @@ROWCOUNT,
					errorMessage = null
				where Name = @Name
		end
	end try

	begin catch
		update dbo.fileTransfer
			set DateOperation = getdate(),
				LoadCount = null,
				errorMessage = @@ERROR
			where Name = @Name
	end catch

	set @Name = null

	set @rowStep = @rowStep + 1	
end			

return

GO
