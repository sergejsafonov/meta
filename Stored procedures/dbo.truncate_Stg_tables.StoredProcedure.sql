USE [meta]
GO
/****** Object:  StoredProcedure [dbo].[truncate_Stg_tables]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[truncate_Stg_tables]
	@schemaName NVARCHAR(128) = NULL,
	@entityName NVARCHAR(128) = NULL,
	@typeLoad NVARCHAR(128) = NULL
-- ,@isSync BIT = 1 /*Добавлен параметр. Используется в вызове процедуры meta.dbo.InsertFromSource_openQuery со значением @isSync = 0, 
--						чтобы не вызывать дедлоки со стороны массовых параллельных контейнеров, в которых выполняется вставка через meta.dbo.InsertFromSource_openQuery
--						*/
AS

declare @rowCount int, @rowStep int, @selectStr nvarchar(max), @entityID int, @isPOS bit
/*
IF @isSync = 1
	EXEC dbo.sync_entity_STG;
*/	
BEGIN TRY
	-- определим количество иттераций по создаваемым таблицам для цикла
	if (@entityName is not null and @schemaName is not null)
		select @rowCount = 1
	ELSE
		SELECT @rowCount = COUNT(*) 
			FROM dbo.entity_STG e
			WHERE e.isUse = 1
				AND e.schemaName <> 'ecom'
				AND (@typeLoad IS NULL 
					OR @typeLoad = 'everyHour' AND e.isEveryHour = 1
					OR @typeLoad = 'incremental' AND e.isIncremental = 1
					OR @typeLoad = 'nightFull' AND e.isNightFull = 1
					OR @typeLoad = 'weekFull' AND e.isWeekFull = 1)

	set @rowStep = 1
	WHILE (@rowStep <= @rowCount)
	BEGIN
		IF (@rowStep = 1 AND @entityName IS NOT NULL AND @schemaName IS NOT NULL)
			SELECT 
					 @entityID = e.entityID
					,@isPOS = e.isPOS
				FROM dbo.entity_STG e
				WHERE e.entityName = @entityName 
					AND e.schemaName = @schemaName 
					AND e.isUse = 1
					AND e.schemaName <> 'ecom'
					AND (@typeLoad IS NULL 
						OR @typeLoad = 'everyHour' AND e.isEveryHour = 1
						OR @typeLoad = 'incremental' AND e.isIncremental = 1
						OR @typeLoad = 'nightFull' AND e.isNightFull = 1
						OR @typeLoad = 'weekFull' AND e.isWeekFull = 1)
		ELSE
			SELECT
				 @entityID = tt.entityID 
				,@entityName = tt.entityName
				,@schemaName = tt.schemaName
				,@isPOS = tt.isPOS
			FROM (SELECT ROW_NUMBER() OVER (ORDER BY e.sortOrder DESC, e.entityID) AS rowNum
						,e.entityID
						,e.entityName
						,e.schemaName
						,e.isPOS
					FROM dbo.entity_STG e
					WHERE e.isUse = 1 AND e.schemaName <> 'ecom'
						AND (@typeLoad IS NULL 
							OR @typeLoad = 'everyHour' AND e.isEveryHour = 1
							OR @typeLoad = 'incremental' AND e.isIncremental = 1
							OR @typeLoad = 'nightFull' AND e.isNightFull = 1
							OR @typeLoad = 'weekFull' AND e.isWeekFull = 1)
				) tt WHERE tt.rowNum = @rowStep
	
	--	if (@isPOS = 1)
	--		set @selectStr = 'use stg IF OBJECT_ID(''' + @schemaName + '.' + @entityName + ''') is not null drop table STG.' + @schemaName + '.' + @entityName
	--	else
		SET @selectStr = 'truncate table STG.[' + @schemaName + '].[' + @entityName + ']'

		EXEC (@selectStr)
--		select @selectStr
		
		SET @rowStep = @rowStep + 1	
	END
END TRY

BEGIN CATCH
	IF (@@ERROR <> 4701)
		THROW;

	SELECT ERROR_MESSAGE()
END CATCH



GO
