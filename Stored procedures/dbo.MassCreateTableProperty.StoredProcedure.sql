USE [meta]
GO
/****** Object:  StoredProcedure [dbo].[MassCreateTableProperty]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--AG
--Use meta;
CREATE Procedure [dbo].[MassCreateTableProperty](
@db nvarchar(100) = 'dwh', @Schema nvarchar(20) = 'sap',@table nvarchar(100), @Attr nvarchar(255), @DefaultValue nvarchar(max) = '' ) 
as begin

DEclare @Result Table(db nvarchar(100), [Schema] nvarchar(20),[table] nvarchar(100), [status] nvarchar(max))

DEclare @sql nvarchar(max);
-- Check Entries
If @db is null or (Select top 1 d.database_id from sys.databases d Where d.name = isnull(@db,'')) is null 
	begin
	Print 'database is empty or not found'
	Print 'DB doesn`t OK'
	Insert @Result VALUES(NULL,NULL,NULL,'db name is empty or not found');
	return;
	end;
Print 'DB OK'

If isnull(@Schema,'') = '' 
begin
	Print 'Schema is null or empty. MultiSchema'
end
Else
begin 
Set @sql =
	'If not exists (Select s.schema_id from '+@db+'.sys.schemas s Where s.name = '''+isnull(@Schema,'')+''') 
	begin
		Print(''schema name not found'') 
	end'
Begin Try
	Exec(@sql);
End Try
Begin CATCH
	Print 'Schema is not found '
	return;
END CATCH
end
Print 'Schema OK'
If isnull(@table,'') ='' 
begin
	Print 'Table is null or empty. MultiTable'
end
Else
begin
Set @sql= '
If (Select top 1 t.object_id from '+@db+'.sys.tables t Where  t.name = '''+isnull(@table,'')+''' and schema_id = (Select top 1 Schema_id from meta.sys.schemas Where name = '''+@Schema+''')) is null 
	begin
	Print ''Table not found''
	end;'
Begin Try 
	Exec(@sql);
end Try
Begin catch
	Print 'Table not found'
	return;
end catch
Print 'Table OK'
end
If isnull(@Attr,'') = ''
Begin
	Print 'Attr is null or empty'
	Print 'Attr doesn`t OK'
	Return
End

Create Table #Tbl(id int, Schema_id int, Schema_name nvarchar(100), Table_id int, Table_name nvarchar(100));
SET @sql =' 
insert into #tbl
	Select row_number() over(Order by s.Schema_id, t.object_id) as id, s.schema_id,s.name,t.object_id, t.name 
	from '+@db+'.sys.schemas s
	inner join '+@db+'.sys.tables t on t.schema_id = s.schema_id
	Where s.[name] like ''%''+'''+isnull(@Schema,'')+'''+''%'' and t.[name] like ''%''+'''+isnull(@table,'')+'''+''%''
'
Exec (@sql);
Select * from #Tbl
--Select @@ROWCOUNT;
DEclare @RowCnt int = 1;
Declare @procedureName nvarchar(100);
Declare @tblname nvarchar(100), @schname nvarchar(100) 
While @RowCnt<=(Select count(id) from #Tbl)
begin
	Select @schname= tt.Schema_name, @tblname = tt.Table_name from #Tbl tt Where @RowCnt = tt.id
	Set @sql = '';
	if (exists (SELECT 1 FROM fn_listextendedproperty (@Attr, 'Schema', @Schname, 'Table', @tblname, default, default)))			
			set @ProcedureName = '1sp_updateextendedproperty'			
		else
			set @ProcedureName = 'sp_addextendedproperty';
	Print @RowCnt;
	set @sql = 	@ProcedureName + ' @name = '''+isnull(@Attr,'Description')+''',@value = N''' + isnull(@DefaultValue,'') + '''' 
	+ ', @level0type = N''Schema'',@level0name = N''' + isnull(@Schname,'') + ''''
	+ ', @level1type = N''Table'',@level1name = N''' + isnull(@tblname,'') + ''''
	Set @sql = 'Use '+@db+ ';' + 'Exec '+ @sql;
--	Select @sql;
    Begin Try
		exec (@sql)
	End Try
	Begin Catch
		Print 'Error property for ' + @schname + '.' + @tblname
		Print @sql
	End Catch
	Set @RowCnt = @RowCnt + 1
end;
drop table #tbl;
End/**/
--Exec meta.dbo.MassCreateTableProperty dwh,dim,null,'Caption SAP'
--Select * from meta.sys.tables Where Schema_id = (Select top 1 Schema_id from meta.sys.schemas Where name = 'dbo')
-- exec sp_addextendedproperty @name = a,@value = N'', @level0type = N'database',@level0name = N'stg', @level1type = N'schema',@level1name = N'sap',  @level2type = N'Table',@level2name = N'KBLK'
--Use stg;
--Exec sp_addextendedproperty @name = a,@value = N'', @level0type = N'Schema',@level0name = N'sap', @level1type = N'Table',@level1name = N'KBLK'

GO
