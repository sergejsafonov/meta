USE [meta]
GO
/****** Object:  StoredProcedure [dbo].[SAPFieldValues]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Use meta
-- AG
Create Procedure [dbo].[SAPFieldValues](@SapTab varchar(160) = 'T001')
-- процедура выполняет поиск уникальных значений для полей таблицы SAP
-- Внимание! Для таблицы с большим количеством полей и колонок выполнение может быть долгим
AS
--DROP INDEX [ccsi-dd03l] ON [sap].[dd03l]
----------------------
Declare 
--@SapTab varchar(160) = 'T001',
@Buf varchar(160) = '',
@sql varchar(6000) = ''
create table #FCount (txt varchar(160), dcount int)

DECLARE @CURSOR CURSOR
SET @CURSOR  = CURSOR SCROLL
FOR
SELECT  fieldname from stg.sap.dd03l l WHere  l.Tabname = @Saptab
Order by position
Open @cursor
Fetch next from @cursor into @buf
WHILE @@FETCH_STATUS = 0 
BEGIN
If patindex('%.%',@Buf) = 0 
Begin
Set @sql =  
'SELECT '''+@saptab+'.'+@Buf+''' as FieldName, DistCount
 FROM OPENQUERY(SAP, ''Select Count(distinct '+@buf+') as DistCount from sapzrp.'+@saptab+' b where mandt = ''''600'''' with ur'')'
 print @Buf
 Print @sql
insert into #FCount
exec (@SQL)
end
FETCH NEXT FROM @CURSOR INTO @buf
END
CLOSE @CURSOR
Select * from #FCount Order by 2 desc
drop table #FCount
--CREATE CLUSTERED COLUMNSTORE INDEX [ccsi-dd03l] ON [sap].[dd03l] WITH (DROP_EXISTING = OFF, COMPRESSION_DELAY = 0) ON [PRIMARY]
Return;


GO
