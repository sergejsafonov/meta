USE [meta]
GO
/****** Object:  View [msdb].[v_GetStatusJobs]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [msdb].[v_GetStatusJobs]
as
SELECT 
     CASE [sJSTP].[last_run_date]
        WHEN 0 THEN NULL
        ELSE 
            CAST(
                CAST([sJSTP].[last_run_date] AS CHAR(8))
                + ' ' 
                + STUFF(
                    STUFF(RIGHT('000000' + CAST([sJSTP].[last_run_time] AS VARCHAR(6)),  6)
                        , 3, 0, ':')
                    , 6, 0, ':')
                AS DATETIME)
      END AS [LastRunDateTime]
	 ,case when [sJOB].[name]='Incremental Load' then 'LoadIncremental'
			when [sJOB].[name]='Night full day Load' then 'LoadNightFull'
			when [sJOB].[name]='Week full fact Load' then 'LoadWeekFull'
	 end AS [JobName]
--	, [sJSTP].[step_id] AS [StepNo]
--    , [sJSTP].[step_name] AS [StepName]
	, CASE [sJSTP].[last_run_outcome]
        WHEN 0 THEN 'Failed'
        WHEN 1 THEN 'Succeeded'
        WHEN 2 THEN 'Retry'
        WHEN 3 THEN 'Canceled'
		WHEN 4 THEN 'Running' -- In Progress
        WHEN 5 THEN 'Unknown'
      END AS [LastRunStatus]
/*	, CASE [sJSTP].[subsystem]
        WHEN 'ActiveScripting' THEN 'ActiveX Script'
        WHEN 'CmdExec' THEN 'Operating system (CmdExec)'
        WHEN 'PowerShell' THEN 'PowerShell'
        WHEN 'Distribution' THEN 'Replication Distributor'
        WHEN 'Merge' THEN 'Replication Merge'
        WHEN 'QueueReader' THEN 'Replication Queue Reader'
        WHEN 'Snapshot' THEN 'Replication Snapshot'
        WHEN 'LogReader' THEN 'Replication Transaction-Log Reader'
        WHEN 'ANALYSISCOMMAND' THEN 'SQL Server Analysis Services Command'
        WHEN 'ANALYSISQUERY' THEN 'SQL Server Analysis Services Query'
        WHEN 'SSIS' THEN 'SQL Server Integration Services Package'
        WHEN 'TSQL' THEN 'Transact-SQL script (T-SQL)'
        ELSE sJSTP.subsystem
      END AS [StepType]*/
--	, [sJSTP].[database_name] AS [Database]
--  , [sJSTP].[command] AS [ExecutableCommand]
    , STUFF(
            STUFF(RIGHT('000000' + CAST([sJSTP].[last_run_duration] AS VARCHAR(6)),  6)
                , 3, 0, ':')
            , 6, 0, ':')
      AS [LastRunDuration (HH:MM:SS)]
    , [sJOBH].[message] AS [LastRunStatusMessageOfJOB]
    , CASE [sJOBSCH].[NextRunDate]
        WHEN 0 THEN NULL
        ELSE CAST(
                CAST([sJOBSCH].[NextRunDate] AS CHAR(8))
                + ' ' 
                + STUFF(
                    STUFF(RIGHT('000000' + CAST([sJOBSCH].[NextRunTime] AS VARCHAR(6)),  6)
                        , 3, 0, ':')
                    , 6, 0, ':')
                AS DATETIME)
      END AS [NextRunDateTimeJOB]
FROM 
    [msdb].[dbo].[sysjobs] AS [sJOB]
	INNER JOIN [msdb].[dbo].[sysjobsteps] AS [sJSTP]
		on [sJOB].[job_id]=[sJSTP].[job_id]
    INNER JOIN (
                SELECT
                    [sSCH].[job_id]
                    , MIN([sSCH].[next_run_date]) AS [NextRunDate]
                    , MIN([sSCH].[next_run_time]) AS [NextRunTime]
                FROM [msdb].[dbo].[sysjobschedules] [sSCH]
				where [sSCH].[schedule_id] is not null
                GROUP BY [job_id]
            ) AS [sJOBSCH]
        ON [sJOB].[job_id] = [sJOBSCH].[job_id]
    LEFT JOIN (
                SELECT 
                    [job_id]
                    , [run_date]
                    , [run_time]
                    , [run_status]
                    , [run_duration]
                    , [message]
                    , ROW_NUMBER() OVER (
                                            PARTITION BY [job_id] 
                                            ORDER BY [run_date] DESC, [run_time] DESC
                      ) AS RowNumber
                FROM [msdb].[dbo].[sysjobhistory]
                WHERE [step_id] = 0
            ) AS [sJOBH]
        ON [sJOB].[job_id] = [sJOBH].[job_id]
        AND [sJOBH].[RowNumber] = 1
WHERE [sJOB].[enabled]=1 /*Хардкод для отобора 3 типов загрузки идущих вторым шагом в джобе*/
and [sJSTP].[step_id]=2 and [sJOB].[name] in ('Incremental Load','Night full day Load','Week full fact Load')

GO
