USE [meta]
GO
/****** Object:  View [dbo].[v_Table_Descriptions]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create VIEW [dbo].[v_Table_Descriptions]

AS 

SELECT s.name as schemaName
     , t.name as tableName
	 , e.name as propertyName
     , e.value as extendedProperties
  FROM dwh.sys.schemas s
 left JOIN dwh.sys.tables t
    ON s.schema_id = t.schema_id
 left JOIN dwh.sys.extended_properties e
    ON t.object_id = e.major_id
Where e.[minor_id]=0




GO
