USE [meta]
GO
/****** Object:  View [dbo].[v_Field_Descriptions]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE VIEW [dbo].[v_Field_Descriptions]

AS 

SELECT dwh.sys.schemas.name schemaName
     , dwh.sys.tables.name tableName
     , dwh.sys.columns.name columnName
	 , dwh.sys.extended_properties.name propertyName
     , dwh.sys.extended_properties.value extendedProperties
  FROM dwh.sys.schemas
 left JOIN dwh.sys.tables
    ON schemas.schema_id = tables.schema_id
 left JOIN dwh.sys.columns
    ON tables.object_id = columns.object_id
 left JOIN dwh.sys.extended_properties
    ON tables.object_id = extended_properties.major_id
   AND columns.column_id = extended_properties.minor_id






GO
