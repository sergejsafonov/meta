USE [meta]
GO
/****** Object:  View [dbo].[v_DWH_STG_by_PROC3]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE View [dbo].[v_DWH_STG_by_PROC3] 
as 
Select distinct a.DWH_Schema, a.DWH_Table, a.stg_table as STG_Table, d.attributeName as DWH_Field--, s.attributeName as STG_Field 
from 
(Select distinct 
dwh_schema, 
dwh_table, 
Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(STG_Table,'[',''),']',''),'stg.',''),'sap.',''),'crm.',''),'set_.',''),'posr.',''),'ecom.',''),'rstat',''),'loyal.',''),'stg1.',''),'.','') as stg_table
from meta.[dbo].[v_DWH_STG_by_PROC]) a
left join meta.dbo.attribute_DWH d On d.entityName = a.DWH_Table
left join meta.dbo.attribute_STG s On s.entityName = a.STG_Table
/*
left join (SELECT dwh.sys.schemas.name schemaName
				, dwh.sys.tables.name tableName
				, dwh.sys.columns.name columnName
				, dwh.sys.extended_properties.name propertyName
				, dwh.sys.extended_properties.value extendedProperties
			FROM dwh.sys.schemas
			left JOIN dwh.sys.tables ON schemas.schema_id = tables.schema_id
			left JOIN dwh.sys.columns ON tables.object_id = columns.object_id
			left JOIN dwh.sys.extended_properties ON tables.object_id = extended_properties.major_id AND columns.column_id = extended_properties.minor_id
			) t on t.schemaName = a.DWH_Schema and t.tableName = a.DWH_Table and t.columnName = d.attributeName
*/


GO
