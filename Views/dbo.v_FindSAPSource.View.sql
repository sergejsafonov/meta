USE [meta]
GO
/****** Object:  View [dbo].[v_FindSAPSource]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







--Exec [dbo].[FindSAPSource] 'Брак'

CREATE View [dbo].[v_FindSAPSource] 
AS

Select distinct d4l.RollName,d4t.DDTEXT, d4t.REPTEXT, d4t.SCRTEXT_L,
d3l.TABNAME, d3l.FIELDNAME,  d3l.Position, d3l.REFTABLE, d3l.REFFIELD
from stg.sap.dd04l d4l
left join stg.sap.dd04T d4t on d4t.ROLLNAME = d4l.ROLLNAME and d4l.AS4LOCAL = d4t.AS4LOCAL and d4l.as4vers = d4t.AS4VERS
inner join stg.sap.dd03l d3l on d3l.AS4LOCAL = d3l.AS4LOCAL and d3l.AS4VERS = d3l.AS4VERS and d3l.ROLLNAME = d4l.RollName
--left join stg.sap.dd03t d3t on d3t.TABNAME = d3l.TABNAME and d3t.FIELDNAME = d3l.FIELDNAME and d3t.AS4LOCAL = d3l.AS4LOCAL
Where D4T.DDLanguage = ('R') 
and not (d4l.RollName like '%/%)') 
and not(d3l.REFTABLE like '%/%')
--and isnull(d4t.DDTEXT,'') like '%%'














GO
