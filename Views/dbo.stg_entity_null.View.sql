USE [meta]
GO
/****** Object:  View [dbo].[stg_entity_null]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[stg_entity_null]
as
Select [Schema] as SchemaName, [Table] as TableName, object_id as [internal_Object_ID], EntityName from 
(Select s.name as [Schema], o.name as [Table], o.object_id, estg.entityName from stg.sys.objects o 
inner join stg.sys.schemas s on o.schema_id = s.schema_id
full join meta.dbo.entity_STG estg on estg.object_id = o.object_id
Where o.[type]  = 'U') a
Where a.entityName is null

GO
