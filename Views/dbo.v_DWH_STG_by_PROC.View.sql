USE [meta]
GO
/****** Object:  View [dbo].[v_DWH_STG_by_PROC]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE View [dbo].[v_DWH_STG_by_PROC] 
as 
Select * from dbo.[DWH_STG_Connection_by_Stored_Proc]() a Where patindex('%stg%',a.[STG_Table])>0


GO
