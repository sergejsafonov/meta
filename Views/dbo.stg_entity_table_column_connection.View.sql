USE [meta]
GO
/****** Object:  View [dbo].[stg_entity_table_column_connection]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[stg_entity_table_column_connection]
as

Select s.name as [Schema], o.name as [Table], c.name, estg.entityName, astg.attributeName  
from stg.sys.objects o 
inner join stg.sys.schemas s on o.schema_id = s.schema_id
inner join stg.sys.columns c on c.object_id = o.object_id
full join dbo.entity_STG estg on estg.object_id = o.object_id
full join meta.dbo.attribute_STG astg on astg.entityID = estg.entityID and astg.attributeName = c.name
Where o.[type]  = 'U'




GO
