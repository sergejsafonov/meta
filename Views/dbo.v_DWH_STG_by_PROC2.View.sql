USE [meta]
GO
/****** Object:  View [dbo].[v_DWH_STG_by_PROC2]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



Create View [dbo].[v_DWH_STG_by_PROC2] 
as 
Select distinct a.DWH_Schema, a.DWH_Table, a.stg_table as STG_Table, d.attributeName as DWH_Field, s.attributeName as STG_Field from 
(Select distinct 
dwh_schema, 
dwh_table, 
Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(STG_Table,'[',''),']',''),'stg.',''),'sap.',''),'crm.',''),'set_.',''),'posr.',''),'ecom.',''),'rstat',''),'loyal.',''),'stg1.','') as stg_table
from meta.[dbo].[v_DWH_STG_by_PROC]) a
left join meta.dbo.attribute_DWH d On d.entityName = a.DWH_Table
left join meta.dbo.attribute_STG s On s.entityName = a.stg_table



GO
