USE [meta]
GO
/****** Object:  View [dbo].[dwh_entity_table_column_connection]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[dwh_entity_table_column_connection]
as

Select s.name as [Schema], o.name as [Table], c.name as ColumnName, edwh.EntityName, adwh.AttributeName  
from dwh.sys.objects o 
inner join dwh.sys.schemas s on o.schema_id = s.schema_id
inner join dwh.sys.columns c on c.object_id = o.object_id
full join dbo.entity_dwh edwh on edwh.object_id = o.object_id
full join meta.dbo.attribute_DWH adwh on adwh.entityID = edwh.entityID and adwh.attributeName = c.name
Where o.[type]  = 'U'




GO
