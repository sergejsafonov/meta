USE [meta]
GO
/****** Object:  UserDefinedFunction [dbo].[STG_EntityList]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Function [dbo].[STG_EntityList]() Returns @Result Table(	[entityID] [int] ,
	[schemaName] [nvarchar](128),
	[entityName] [nvarchar](128),
	[addQueryStr] [nvarchar](1024),
	[sortOrder] [smallint],
	[isClass] [bit] ,
	[isMaterial] [bit] ,
	[isCompany] [bit] ,
	[isFact] [bit] ,
	[isPOS] [bit] ,
	[isUse] [bit] ,
	[isIncremental] [bit] ,
	[isNightFull] [bit] ,
	[isWeekFull] [bit] ,
	[isAddQueryStrIncremental] [bit] ,
	[isDocumentType] [bit] ,
	[clientField] [nvarchar](32) ,
	[isCRM] [bit] ,
	[isFullDelete] [bit] ,
	[isPOSSet] [bit] ,
	[isPOSLoy] [bit] ,
	[isRstat] [bit] ,
	[isTreasury] [bit] ,
	[isUseSlidingDate] [bit] ,
	[schemaAlias] [nvarchar](128),
	[isEveryHour] [bit] ,
	[isFinance] [bit] ,
	[isAxPOS] [bit],
	SlidingDate datetime,
	dwh_Table nvarchar(128),
	etl_Func nvarchar(128))
as begin
insert @Result
Select  es.[entityID],
	es.[schemaName],
	es.[entityName],
	es.[addQueryStr],
	es.[sortOrder],
	es.[isClass],
	es.[isMaterial],
	es.[isCompany],
	es.[isFact],
	es.[isPOS],
	es.[isUse],
	es.[isIncremental],
	es.[isNightFull],
	es.[isWeekFull],
	es.[isAddQueryStrIncremental],
	es.[isDocumentType],
	es.[clientField],
	es.[isCRM],
	es.[isFullDelete],
	es.[isPOSSet],
	es.[isPOSLoy],
	es.[isRstat],
	es.[isTreasury],
	es.[isUseSlidingDate],
	es.[schemaAlias],
	es.[isEveryHour],
	es.[isFinance],
	es.[isAxPOS], 
	dt.dt as SlidingDate, uz.* from meta.dbo.entity_STG es
left join meta.dbo.SlidingDate sd on sd.EntityName = es.entityName
cross apply (select meta.dbo.fn_get_SlidingDate(es.schemaName,es.entityName,'incremental' ) as dt) dt
cross apply (Select conn.DWH_Schema+'.'+conn.DWH_Table as tbl, conn.PROC_Name from meta.dbo.DWH_STG_Connection_by_Stored_Proc() conn 
	Where es.entityName = Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(
							conn.STG_Table,'[',''),'[',''),'stg.',''),'dbo.',''),'sap.',''),'ecom.',''),'crm.',''),'pos.',''),'posr.',''),'posb.',''),'loyal.','')
	) uz
return
end




GO
