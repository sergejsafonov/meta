USE [meta]
GO
/****** Object:  UserDefinedFunction [dbo].[DWH_STG_Connection_by_Stored_Proc]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE Function [dbo].[DWH_STG_Connection_by_Stored_Proc]() Returns @Result Table(DWH_Schema nvarchar(10),PROC_Name nvarchar(100),DWH_Table nvarchar(150),STG_Table nvarchar(150))
as begin
insert @Result
Select distinct * from (
SELECT 
SCHEMA_NAME(o.schema_id) as 'DWH SCHEMA NAME', 
o.name AS 'PROCEDURE NAME',
Replace(Replace(o.name,'_Merge',''),'_upload','') AS 'DWH TABLE'
--,substring(c.text, PATindex('%from %',c.text) + 5, ABS(Patindex('% %',substring(c.text, PATindex('%from %',c.text)+5,50))-1)) + '$' as 'k0'
--,PATindex('%from %',c.text) as m0
--,Patindex('% %',substring(c.text, PATindex('%from %',c.text)+5,50)) m1
--,patindex('%.%',substring(c.text, PATindex('%from %',c.text) + 5, ABS(Patindex('% %',substring(c.text, PATindex('%from %',c.text)+5,50))-1))) as m2
--,c.text as Script
,left(
Replace(
Replace(
substring(c.text, PATindex('%from %',c.text) + 5, ABS(Patindex('% %',substring(c.text, PATindex('%from %',c.text)+5,50))-1)),'	',' '),'
',' '),
PATindex('% %',
Replace(Replace(substring(c.text, PATindex('%from %',c.text) + 5, ABS(Patindex('% %',substring(c.text, PATindex('%from %',c.text)+5,50))-1)),'	',' '),'
',' ')
+ '' + ' ')
) as 'STG TABLE'
FROM dwh.sys.objects o
left JOIN dwh.sys.syscomments c ON c.id = o.object_id 
--inner join dwh.syscomments c1 on c1.id = o.object_id
WHERE (o.name like '%merge%' or o.name like '%upload%') and PATindex('%from %',c.text)<>0
and not (substring(c.text, PATindex('%from %',c.text)-10,100) like '%delete%')
group by colid,o.schema_id,o.name,c.text
Having colid = max(colid)) a
Return;
end




GO
EXEC sys.sp_addextendedproperty @name=N'Description', @value=N'Связка таблиц DWH и STG по текстам хранимых процедур MERGE или UPLOAD в DWH.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'FUNCTION',@level1name=N'DWH_STG_Connection_by_Stored_Proc'
GO
