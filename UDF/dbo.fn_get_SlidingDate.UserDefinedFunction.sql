USE [meta]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_get_SlidingDate]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create function [dbo].[fn_get_SlidingDate]
	(@SchemaName nvarchar(128), 
	 @EntityName nvarchar(128),
	 @typeLoad nvarchar(32) = 'incremental')

returns datetime2(7)
as
begin
	declare @SlidingDate datetime2(7), @SlidingValue smallint, @SlidingValueNight smallint, @SlidingValueWeek smallint
	
	select @SlidingValue = SlidingValue,
			@SlidingValueNight = SlidingValueNight,
			@SlidingValueWeek = SlidingValueWeek
		from dbo.SlidingDate 
		where EntityName = @EntityName and SchemaName = @SchemaName
	
	if (@typeLoad in ('incremental','everyHour'))	
		set @SlidingDate = dateadd(day, -@SlidingValue, cast(getdate() as date))
	else if (@typeLoad = 'nightFull')
		set @SlidingDate = dateadd(day, -@SlidingValueNight, cast(getdate() as date))
	else if (@typeLoad = 'weekFull')
		set @SlidingDate = dateadd(day, -@SlidingValueWeek, cast(getdate() as date))

	return @SlidingDate
end

GO
