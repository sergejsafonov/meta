USE [meta]
GO
/****** Object:  UserDefinedFunction [dbo].[DWH_STG_Connection_by_Stored_Proc1]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Function [dbo].[DWH_STG_Connection_by_Stored_Proc1]() Returns @Result Table(DWH_Schema nvarchar(10),PROC_Name nvarchar(100),DWH_Table nvarchar(150),STG_Table nvarchar(150),[text] ntext)
as begin
insert @Result
Select distinct * from (
SELECT 
SCHEMA_NAME(o.schema_id) as 'DWH SCHEMA NAME', 
o.name AS 'PROCEDURE NAME',
Replace(Replace(o.name,'_Merge',''),'_upload','') AS 'DWH TABLE',
left(
Replace(
Replace(
substring(c.text, PATindex('%from %',c.text) + 5, Patindex('% %',substring(c.text, PATindex('%from %',c.text)+5,50))-1),'	',' '),'
',' ')
+ '',PATindex('% %',
Replace(Replace(substring(c.text, PATindex('%from %',c.text) + 5, Patindex('% %',substring(c.text, PATindex('%from %',c.text)+5,50))-1),'	',' '),'
',' ')
+ '' + ' ')) as 'STG TABLE',
c.text
FROM dwh.sys.objects o
left JOIN dwh.sys.syscomments c ON c.id = o.object_id 
--inner join dwh.syscomments c1 on c1.id = o.object_id
WHERE c.number = 1 and  (o.name like '%merge%' or o.name like '%upload%') and PATindex('%from %',c.text)<>0
and not (substring(c.ctext, PATindex('%from %',c.text)-10,100) like '%delete%')
group by colid,o.schema_id,o.name,c.text
Having colid = max(colid)) a
Return;
end
GO
