USE [meta]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_get_max_changeDate]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create function [dbo].[fn_get_max_changeDate]
	(@SchemaName nvarchar(128), 
	 @EntityName nvarchar(128))

returns date
as
begin
	declare @MaxChangeDate date, @MaxChangeDateTimeID bigint
	
	select @MaxChangeDate = cast(dateadd(day,-DeltaDays,MaxChangeDate) as date),
			@MaxChangeDateTimeID = MaxChangeDateTimeID
		from dbo.max_changeDate
		where entityName = @EntityName and schemaName = @SchemaName
	
	return @MaxChangeDate
end

GO
