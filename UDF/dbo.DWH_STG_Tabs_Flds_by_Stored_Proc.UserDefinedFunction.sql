USE [meta]
GO
/****** Object:  UserDefinedFunction [dbo].[DWH_STG_Tabs_Flds_by_Stored_Proc]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Function [dbo].[DWH_STG_Tabs_Flds_by_Stored_Proc]() Returns @Result Table(DWH_Schema nvarchar(10),DWH_Table nvarchar(150),STG_Table nvarchar(150),[text] ntext,connectVal nvarchar(max), stg_Field nvarchar(255), dwh_Field nvarchar(255))
as begin
insert @Result
Select stage1.*,
replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(
replace(replace(replace(replace(replace(replace(replace(replace(replace(
replace(replace(replace(replace(replace(replace(replace(replace(
left([value],len(value)-
(PATINDEX('% sa %',REVERSE(value))+3))
,'try_convert(',''),'isnull(',''),'cast(',''),')',''),'(',''),'[',''),']',''),'''','')
,'as date',''),'as smallint,',''),'as int',''),'as bigint',''), 'as nvarchar',''), 'as varchar',''), 'as money',''),'as float',''),'as varbinary','')
,',',''),'charindex',''),'datetime',''),'float',''),'money',''),'smallint',''),'tinyint',''),'coalesce',''),'nullif',''),'concat',''),'''00000000''',''),'''20000101''',''),'replace',''),' as ','')
as STG_Field_Predicate,
replace(replace(replace(
right([value],
(PATINDEX('% sa %',REVERSE(value))))
,',',''),'[',''),']','')
as DWH_Field_Predicate
from( 
Select *
/*================================================================*/
from (
SELECT 
SCHEMA_NAME(o.schema_id) as 'DWH SCHEMA NAME', 
--o.name AS 'PROCEDURE NAME',
Replace(Replace(o.name,'_Merge',''),'_upload','') AS 'DWH TABLE',
Replace(Replace(Replace(Replace(
left(
Replace(Replace(
substring(c.text, PATindex('%from %',c.text) + 5, Patindex('% %',substring(c.text, PATindex('%from %',c.text)+5,50))-1),'	',' '),'
',' ')
+ '',PATindex('% %',
Replace(Replace(substring(c.text, PATindex('%from %',c.text) + 5, Patindex('% %',substring(c.text, PATindex('%from %',c.text)+5,50))-1),'	',' '),'
',' ')
+ '' + ' ')),']',''),'[',''),'stg.',''),'dwh.','') as 'STG TABLE',
ltrim(rtrim(isnull(c.[text],'')+isnull(c1.[text],'')+isnull(c2.[text],''))) as ctext
FROM dwh.sys.objects o
left JOIN dwh.sys.syscomments c ON c.id = o.object_id 
left join dwh.sys.syscomments c1 on c1.id = o.object_id and c1.colid = c.colid+1
left join dwh.sys.syscomments c2 on c2.id = o.object_id and c2.colid = c1.colid+1
WHERE c.number = 1 and  c.colid = 1 and (o.name like '%merge%' or o.name like '%upload%') and PATindex('%from %',c.[text])<>0
and not (substring(c.text, PATindex('%from %',c.[text])-10,100) like '%delete%')
) a
cross apply string_split(ctext,convert(nchar(1),'
')) Where [value] like '% as %' and len([value])>4 and patindex('%from%',[value]) = 0 and patindex('%merge%',[value]) = 0 and patindex('%source%',[value]) = 0 and patindex('%using%',[value]) = 0 and patindex('%select%',[value]) = 0 and patindex('%where%',[value]) = 0 and patindex('%set %',[value]) = 0 and patindex('%THEN%',[value]) = 0 and patindex('%when%',[value]) = 0 and patindex('%join%',[value]) = 0 and patindex('%partition%',[value]) = 0
) stage1
Return;
end

GO
