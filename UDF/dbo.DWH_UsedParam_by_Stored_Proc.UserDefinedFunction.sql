USE [meta]
GO
/****** Object:  UserDefinedFunction [dbo].[DWH_UsedParam_by_Stored_Proc]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Function [dbo].[DWH_UsedParam_by_Stored_Proc](@Param nvarchar(max) = '') Returns @Result Table(DWH_Schema nvarchar(128),PROC_Name nvarchar(128), dateCreate date)
as begin
insert @Result
Select distinct * from (
SELECT 
SCHEMA_NAME(o.schema_id) as 'DWH SCHEMA NAME', 
o.name AS 'PROCEDURE NAME',
o.create_date
FROM dwh.sys.objects o
left JOIN dwh.sys.syscomments c ON c.id = o.object_id 
WHERE (o.name like '%merge%' or o.name like '%upload%') and PATindex('%from %',c.text)<>0
and not (substring(c.text, PATindex('%from %',c.text)-10,100) like '%delete%')
and PATindex('%'+@param+ '%',c.text)>0
group by colid,o.schema_id,o.name,o.create_date, c.text
Having colid = max(colid)) a
Return;
end
GO
