USE [meta]
GO
/****** Object:  UserDefinedFunction [dbo].[DWH_STG_Connection_by_Stored_Proc2]    Script Date: 20.07.2017 12:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Function [dbo].[DWH_STG_Connection_by_Stored_Proc2]() Returns @Result Table(DWH_Schema nvarchar(20),DWH_Table nvarchar(128),STG_Table nvarchar(128),dwhattr nvarchar(128), stgattr nvarchar(128))
as begin
--insert @Result
Declare @sch nvarchar(50), @dwhtab nvarchar(128), @stgtab nvarchar(128), @colid int, @val nvarchar(max), @res nvarchar(max)
DECLARE @CURSOR CURSOR
SET @CURSOR  = CURSOR SCROLL
FOR
--Cursor Source
Select 
[DWH SCHEMA NAME],
[DWH TABLE],
right(Rtrim(Replace(Replace(Replace(Replace([STG TABLE],'[',''),']',''),'dwh.',''),'stg.','')),
len(Rtrim(Replace(Replace(Replace(Replace([STG TABLE],'[',''),']',''),'dwh.',''),'stg.','')))-
PATINDEX('%.%',Rtrim(Replace(Replace(Replace(Replace([STG TABLE],'[',''),']',''),'dwh.',''),'stg.','')))) as [STG TABLE],
colid,
[text]
from (
Select distinct * from (
SELECT 
SCHEMA_NAME(o.schema_id) as 'DWH SCHEMA NAME', 
--o.name AS 'PROCEDURE NAME',
Replace(Replace(o.name,'_Merge',''),'_upload','') AS 'DWH TABLE',
left(
Replace(
Replace(
substring(c.text, PATindex('%from %',c.text) + 5, Patindex('% %',substring(c.text, PATindex('%from %',c.text)+5,50))-1),'	',' '),'
',' ')
+ '',PATindex('% %',
Replace(Replace(substring(c.text, PATindex('%from %',c.text) + 5, Patindex('% %',substring(c.text, PATindex('%from %',c.text)+5,50))-1),'	',' '),'
',' ')
+ '' + ' ')) as 'STG TABLE',
c.colid,
c.text
FROM dwh.sys.objects o
left JOIN dwh.sys.syscomments c ON c.id = o.object_id 
WHERE c.number = 1 and  (o.name like '%merge%' or o.name like '%upload%') and PATindex('%from %',c.text)<>0
and not (substring(c.ctext, PATindex('%from %',c.text)-10,100) like '%delete%')
and SCHEMA_NAME(o.schema_id) is not null
group by colid,o.schema_id,o.name,c.text
)a) cmts
-- Cursor Source
Open @cursor
Fetch next from @cursor into @sch, @dwhtab, @stgtab, @colid, @val
WHILE @@FETCH_STATUS = 0
BEGIN
Select @res = @sch + '-' + @dwhtab+'-'+@stgtab+'-'+convert(nvarchar,@colid)+'-'+ @val;


FETCH NEXT FROM @CURSOR INTO @sch, @dwhtab, @stgtab, @colid, @val
END
CLOSE @CURSOR
Return;
end
GO
