USE [meta]
GO
/****** Object:  Table [dbo].[entity_SSD]    Script Date: 27.07.2017 12:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[entity_SSD](
	[entityID] [int] IDENTITY(1,1) NOT NULL,
	[schemaName] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[entityName] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[alias] [nvarchar](8) COLLATE Cyrillic_General_CI_AS NULL,
	[object_id] [int] NULL,
	[sortOrder] [smallint] NULL,
	[isFact] [bit] NOT NULL,
	[isUse] [bit] NOT NULL,
	[caption] [nvarchar](32) COLLATE Cyrillic_General_CI_AS NULL,
	[description] [nvarchar](256) COLLATE Cyrillic_General_CI_AS NULL,
	[isIncremental] [bit] NOT NULL,
	[isXLSI] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[entityID] ASC
)WITH (PAD_INDEX = ON, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ui_entity_ssd_schemaName_entityName]    Script Date: 27.07.2017 12:19:20 ******/
CREATE UNIQUE NONCLUSTERED INDEX [ui_entity_ssd_schemaName_entityName] ON [dbo].[entity_SSD]
(
	[schemaName] ASC,
	[entityName] ASC
)WITH (PAD_INDEX = ON, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
ALTER TABLE [dbo].[entity_SSD] ADD  DEFAULT ((0)) FOR [isFact]
GO
ALTER TABLE [dbo].[entity_SSD] ADD  DEFAULT ((1)) FOR [isUse]
GO
ALTER TABLE [dbo].[entity_SSD] ADD  CONSTRAINT [DF__entity_SS__isInc__1F798287]  DEFAULT ((0)) FOR [isIncremental]
GO
ALTER TABLE [dbo].[entity_SSD] ADD  DEFAULT ((0)) FOR [isXLSI]
GO
