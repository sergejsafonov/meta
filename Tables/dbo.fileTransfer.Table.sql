USE [meta]
GO
/****** Object:  Table [dbo].[fileTransfer]    Script Date: 27.07.2017 12:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fileTransfer](
	[fileTransferID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](32) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[isReverse] [bit] NOT NULL,
	[dbName] [nvarchar](16) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[schemaName] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[entityName] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[resultEntity]  AS (concat([dbName],'.',[schemaName],'.',[entityName])),
	[entityID] [int] NULL,
	[localFilePath] [nvarchar](256) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[localFileName] [nvarchar](64) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[DynamicFileValue] [nvarchar](256) COLLATE Cyrillic_General_CI_AS NULL,
	[ResultDynamicFileValue] [nvarchar](64) COLLATE Cyrillic_General_CI_AS NULL,
	[isXLS] [bit] NOT NULL,
	[fileType] [nvarchar](16) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[fileProvider] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NULL,
	[ProviderExtProperty] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NULL,
	[SheetName] [nvarchar](64) COLLATE Cyrillic_General_CI_AS NULL,
	[ConnectionString] [nvarchar](1024) COLLATE Cyrillic_General_CI_AS NULL,
	[collationCode] [nvarchar](8) COLLATE Cyrillic_General_CI_AS NULL,
	[collation] [nvarchar](32) COLLATE Cyrillic_General_CI_AS NULL,
	[codepage] [nvarchar](32) COLLATE Cyrillic_General_CI_AS NULL,
	[isUnicode] [bit] NOT NULL,
	[datefiletype] [nvarchar](32) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[isKeepNulls] [bit] NOT NULL,
	[FirstRow] [tinyint] NOT NULL,
	[FieldTerminator] [nvarchar](8) COLLATE Cyrillic_General_CI_AS NULL,
	[RowTerminator] [nvarchar](8) COLLATE Cyrillic_General_CI_AS NULL,
	[isExternalSource] [bit] NOT NULL,
	[ExternalHost1] [nvarchar](1024) COLLATE Cyrillic_General_CI_AS NULL,
	[ExternalHost2] [nvarchar](1024) COLLATE Cyrillic_General_CI_AS NULL,
	[DynamicPathValue] [nvarchar](1024) COLLATE Cyrillic_General_CI_AS NULL,
	[ResultDynamicPathValue] [nvarchar](64) COLLATE Cyrillic_General_CI_AS NULL,
	[isFtpHost] [bit] NOT NULL,
	[ftpPort] [smallint] NULL,
	[ftpUser] [nvarchar](32) COLLATE Cyrillic_General_CI_AS NULL,
	[ftpPass] [nvarchar](32) COLLATE Cyrillic_General_CI_AS NULL,
	[FtpHost]  AS (case when [isFtpHost]=(1) then [ExternalHost1]  end),
	[isUse] [bit] NOT NULL,
	[isIncremental] [bit] NOT NULL,
	[isNightFull] [bit] NOT NULL,
	[DateOperation] [datetime2](7) NOT NULL,
	[LoadCount] [int] NULL,
	[errorMessage] [nvarchar](1024) COLLATE Cyrillic_General_CI_AS NULL,
	[IntegrationFilePath] [nvarchar](256) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[localHost]  AS (concat([IntegrationFilePath],[localFilePath],[localFileName],isnull([ResultDynamicFileValue],''),'.',[fileType])),
	[isGetReal] [bit] NOT NULL,
	[ExternalHost]  AS (case when [isExternalSource]=(1) then concat([ExternalHost1],isnull([ResultDynamicPathValue],''),isnull([ExternalHost2],''),case when [isGetReal]=(0) then '.'+[localFileName] else '' end,isnull([ResultDynamicFileValue],''),case when [isGetReal]=(0) then '.'+[fileType] else '' end)  end),
PRIMARY KEY CLUSTERED 
(
	[fileTransferID] ASC
)WITH (PAD_INDEX = ON, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = ON, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[fileTransfer] ADD  DEFAULT ((0)) FOR [isReverse]
GO
ALTER TABLE [dbo].[fileTransfer] ADD  DEFAULT ((0)) FOR [isXLS]
GO
ALTER TABLE [dbo].[fileTransfer] ADD  DEFAULT ('ACP') FOR [codepage]
GO
ALTER TABLE [dbo].[fileTransfer] ADD  DEFAULT ((0)) FOR [isUnicode]
GO
ALTER TABLE [dbo].[fileTransfer] ADD  DEFAULT ('char') FOR [datefiletype]
GO
ALTER TABLE [dbo].[fileTransfer] ADD  DEFAULT ((1)) FOR [isKeepNulls]
GO
ALTER TABLE [dbo].[fileTransfer] ADD  DEFAULT ((1)) FOR [FirstRow]
GO
ALTER TABLE [dbo].[fileTransfer] ADD  DEFAULT ((0)) FOR [isExternalSource]
GO
ALTER TABLE [dbo].[fileTransfer] ADD  DEFAULT ((0)) FOR [isFtpHost]
GO
ALTER TABLE [dbo].[fileTransfer] ADD  DEFAULT ((1)) FOR [isUse]
GO
ALTER TABLE [dbo].[fileTransfer] ADD  DEFAULT ((1)) FOR [isIncremental]
GO
ALTER TABLE [dbo].[fileTransfer] ADD  DEFAULT ((0)) FOR [isNightFull]
GO
ALTER TABLE [dbo].[fileTransfer] ADD  DEFAULT (getdate()) FOR [DateOperation]
GO
ALTER TABLE [dbo].[fileTransfer] ADD  DEFAULT ('\\spb-s-dwh1\integration\') FOR [IntegrationFilePath]
GO
ALTER TABLE [dbo].[fileTransfer] ADD  DEFAULT ((0)) FOR [isGetReal]
GO
ALTER TABLE [dbo].[fileTransfer]  WITH CHECK ADD FOREIGN KEY([entityID])
REFERENCES [dbo].[entity_STG] ([entityID])
GO
