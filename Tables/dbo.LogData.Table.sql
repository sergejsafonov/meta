USE [meta]
GO
/****** Object:  Table [dbo].[LogData]    Script Date: 27.07.2017 12:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogData](
	[LogData] [int] IDENTITY(1,1) NOT NULL,
	[EntityName] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[Operation] [nvarchar](32) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[Host_name] [nvarchar](32) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[User] [nvarchar](32) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[ProcName] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[contextinfo] [varbinary](128) NULL,
	[Cnt] [bigint] NOT NULL,
	[Dt] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[LogData] ASC
)WITH (PAD_INDEX = ON, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
