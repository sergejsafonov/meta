USE [meta]
GO
/****** Object:  Table [dbo].[entity_DWH]    Script Date: 27.07.2017 12:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[entity_DWH](
	[entityID] [int] IDENTITY(1,1) NOT NULL,
	[schemaName] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[entityName] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[alias] [nvarchar](8) COLLATE Cyrillic_General_CI_AS NULL,
	[object_id] [int] NULL,
	[sortOrder] [smallint] NULL,
	[isDim] [bit] NULL,
	[isFact] [bit] NULL,
	[isRelation] [bit] NULL,
	[isService] [bit] NOT NULL,
	[isAddUpdate] [bit] NOT NULL,
	[isClass] [bit] NOT NULL,
	[isMaterial] [bit] NOT NULL,
	[isCompany] [bit] NOT NULL,
	[isColumnStore] [bit] NOT NULL,
	[queryStr] [nvarchar](max) COLLATE Cyrillic_General_CI_AS NULL,
	[isUse] [bit] NOT NULL,
	[isPOS] [bit] NOT NULL,
	[isNightFull] [bit] NOT NULL,
	[isIncremental] [bit] NOT NULL,
	[isWeekFull] [bit] NOT NULL,
	[isDocumentType] [bit] NOT NULL,
	[isDate] [bit] NOT NULL,
	[isResetFlag] [bit] NOT NULL,
	[isCRM] [bit] NOT NULL,
	[isSCD2] [bit] NOT NULL,
	[isTreasury] [bit] NOT NULL,
	[isFinance] [bit] NOT NULL,
	[caption] [nvarchar](64) COLLATE Cyrillic_General_CI_AS NULL,
	[description] [nvarchar](256) COLLATE Cyrillic_General_CI_AS NULL,
	[isDM] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[entityID] ASC
)WITH (PAD_INDEX = ON, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[entity_DWH] ADD  DEFAULT ((0)) FOR [isDim]
GO
ALTER TABLE [dbo].[entity_DWH] ADD  DEFAULT ((0)) FOR [isFact]
GO
ALTER TABLE [dbo].[entity_DWH] ADD  DEFAULT ((0)) FOR [isRelation]
GO
ALTER TABLE [dbo].[entity_DWH] ADD  DEFAULT ((0)) FOR [isService]
GO
ALTER TABLE [dbo].[entity_DWH] ADD  DEFAULT ((0)) FOR [isAddUpdate]
GO
ALTER TABLE [dbo].[entity_DWH] ADD  DEFAULT ((0)) FOR [isClass]
GO
ALTER TABLE [dbo].[entity_DWH] ADD  DEFAULT ((0)) FOR [isMaterial]
GO
ALTER TABLE [dbo].[entity_DWH] ADD  DEFAULT ((0)) FOR [isCompany]
GO
ALTER TABLE [dbo].[entity_DWH] ADD  DEFAULT ((0)) FOR [isColumnStore]
GO
ALTER TABLE [dbo].[entity_DWH] ADD  DEFAULT ((1)) FOR [isUse]
GO
ALTER TABLE [dbo].[entity_DWH] ADD  DEFAULT ((0)) FOR [isPOS]
GO
ALTER TABLE [dbo].[entity_DWH] ADD  DEFAULT ((0)) FOR [isNightFull]
GO
ALTER TABLE [dbo].[entity_DWH] ADD  DEFAULT ((1)) FOR [isIncremental]
GO
ALTER TABLE [dbo].[entity_DWH] ADD  DEFAULT ((0)) FOR [isWeekFull]
GO
ALTER TABLE [dbo].[entity_DWH] ADD  DEFAULT ((0)) FOR [isDocumentType]
GO
ALTER TABLE [dbo].[entity_DWH] ADD  DEFAULT ((0)) FOR [isDate]
GO
ALTER TABLE [dbo].[entity_DWH] ADD  DEFAULT ((1)) FOR [isResetFlag]
GO
ALTER TABLE [dbo].[entity_DWH] ADD  DEFAULT ((0)) FOR [isCRM]
GO
ALTER TABLE [dbo].[entity_DWH] ADD  DEFAULT ((0)) FOR [isSCD2]
GO
ALTER TABLE [dbo].[entity_DWH] ADD  DEFAULT ((0)) FOR [isTreasury]
GO
ALTER TABLE [dbo].[entity_DWH] ADD  DEFAULT ((0)) FOR [isFinance]
GO
ALTER TABLE [dbo].[entity_DWH] ADD  DEFAULT ((0)) FOR [isDM]
GO
