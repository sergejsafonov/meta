USE [meta]
GO
/****** Object:  Table [dbo].[ProverByDay]    Script Date: 27.07.2017 12:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProverByDay](
	[ProverByDayID] [bigint] IDENTITY(1,1) NOT NULL,
	[EntityName] [nvarchar](256) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[DateProver] [date] NULL,
	[DateProverStr] [nchar](8) COLLATE Cyrillic_General_CI_AS NULL,
	[RowSource] [int] NULL,
	[RowDest] [int] NULL,
	[RowDiff] [int] NULL,
	[AmountSource] [money] NULL,
	[AmountDest] [money] NULL,
	[AmountDiff] [money] NULL,
	[DateDiffCur] [int] NULL,
	[EntityDWHName] [nvarchar](255) COLLATE Cyrillic_General_CI_AS NULL,
	[FlagOperation] [tinyint] NOT NULL,
	[DateOperation] [smalldatetime] NOT NULL,
	[isOperate] [bit] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [clu_ProverByDay]    Script Date: 27.07.2017 12:19:20 ******/
CREATE UNIQUE CLUSTERED INDEX [clu_ProverByDay] ON [dbo].[ProverByDay]
(
	[EntityName] ASC,
	[DateProverStr] ASC,
	[DateProver] ASC,
	[isOperate] ASC
)WITH (PAD_INDEX = ON, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ProverByDay] ADD  DEFAULT ((1)) FOR [FlagOperation]
GO
ALTER TABLE [dbo].[ProverByDay] ADD  DEFAULT (getdate()) FOR [DateOperation]
GO
ALTER TABLE [dbo].[ProverByDay] ADD  DEFAULT ((0)) FOR [isOperate]
GO
