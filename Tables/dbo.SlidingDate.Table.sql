USE [meta]
GO
/****** Object:  Table [dbo].[SlidingDate]    Script Date: 27.07.2017 12:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SlidingDate](
	[SchemaName] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[EntityName] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[AttributeName] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[SlidingValue] [smallint] NULL,
	[SlidingValueNight] [smallint] NULL,
	[SlidingValueWeek] [smallint] NULL,
	[SlidingType] [nvarchar](32) COLLATE Cyrillic_General_CI_AS NOT NULL,
 CONSTRAINT [PK_SlidingDate] PRIMARY KEY CLUSTERED 
(
	[SchemaName] ASC,
	[EntityName] ASC
)WITH (PAD_INDEX = ON, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
