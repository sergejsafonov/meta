USE [meta]
GO
/****** Object:  Table [dbo].[entity_SST]    Script Date: 27.07.2017 12:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[entity_SST](
	[entityID] [int] IDENTITY(1,1) NOT NULL,
	[schemaName] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[entityName] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[alias] [nvarchar](8) COLLATE Cyrillic_General_CI_AS NULL,
	[object_id] [int] NULL,
	[sortOrder] [smallint] NULL,
	[isFact] [bit] NOT NULL,
	[isIncremental] [bit] NOT NULL,
	[isNightFull] [bit] NOT NULL,
	[isWeekFull] [bit] NOT NULL,
	[isEveryHour] [bit] NOT NULL,
	[isUse] [bit] NOT NULL,
	[caption] [nvarchar](32) COLLATE Cyrillic_General_CI_AS NULL,
	[description] [nvarchar](256) COLLATE Cyrillic_General_CI_AS NULL,
	[isXLSI] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[entityID] ASC
)WITH (PAD_INDEX = ON, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[entity_SST] ADD  DEFAULT ((0)) FOR [isFact]
GO
ALTER TABLE [dbo].[entity_SST] ADD  DEFAULT ((1)) FOR [isIncremental]
GO
ALTER TABLE [dbo].[entity_SST] ADD  DEFAULT ((0)) FOR [isNightFull]
GO
ALTER TABLE [dbo].[entity_SST] ADD  DEFAULT ((0)) FOR [isWeekFull]
GO
ALTER TABLE [dbo].[entity_SST] ADD  DEFAULT ((0)) FOR [isEveryHour]
GO
ALTER TABLE [dbo].[entity_SST] ADD  DEFAULT ((1)) FOR [isUse]
GO
ALTER TABLE [dbo].[entity_SST] ADD  DEFAULT ((0)) FOR [isXLSI]
GO
