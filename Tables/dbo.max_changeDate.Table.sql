USE [meta]
GO
/****** Object:  Table [dbo].[max_changeDate]    Script Date: 27.07.2017 12:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[max_changeDate](
	[entityID] [int] NOT NULL,
	[schemaName] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[entityName] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[sortOrder] [smallint] NULL,
	[attributeID] [int] NOT NULL,
	[attributeChangeDate] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NULL,
	[attributeChangeDateTime] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NULL,
	[MaxChangeDate] [date] NULL,
	[MaxChangeDateTimeID] [bigint] NULL,
	[DeltaDays] [int] NOT NULL,
	[MaxChangeCode] [bigint] NULL,
	[attributeChangeCode] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NULL,
PRIMARY KEY CLUSTERED 
(
	[entityID] ASC
)WITH (PAD_INDEX = ON, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[max_changeDate] ADD  DEFAULT ((3)) FOR [DeltaDays]
GO
ALTER TABLE [dbo].[max_changeDate]  WITH CHECK ADD FOREIGN KEY([entityID])
REFERENCES [dbo].[entity_DWH] ([entityID])
GO
