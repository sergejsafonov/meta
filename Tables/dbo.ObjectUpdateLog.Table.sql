USE meta
GO
/*
if object_id('dbo.ObjectUpdateLog') is not null
	DROP TABLE meta.dbo.ObjectUpdateLog
go
CREATE TABLE dbo.ObjectUpdateLog(									-- Лог операций обновления
	ObjectUpdateLog int IDENTITY(1,1) NOT NULL PRIMARY KEY CLUSTERED,	-- уникальный идентификатор
	ObjectType NCHAR(1) NOT NULL,										-- P - процедура, T - таблица
	OperationType nvarchar(32) NOT NULL,								-- тип операции (процесса)
	OperationDate datetime NOT NULL DEFAULT (getdate()),				-- дата операции (процесса)
	ObjectName nvarchar(128) NULL,										-- имя процедуры
	LoginName nvarchar(128) NOT NULL,
	HostName nvarchar(128) NOT NULL,
	ProgramName nvarchar(128) NOT NULL
									)
GO
*/

-- alter TABLE dbo.ObjectUpdateLog add test nvarchar(128) NULL
-- alter TABLE dbo.ObjectUpdateLog alter column test nvarchar(118) NULL
-- alter TABLE dbo.ObjectUpdateLog drop column test

SELECT * FROM meta.dbo.ObjectUpdateLog ORDER BY OperationDate DESC
-- DELETE FROM meta.dbo.ObjectUpdateLog WHERE LoginName = 'ZOLOTO585\Safonov.Sergey'