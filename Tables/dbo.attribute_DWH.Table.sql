USE [meta]
GO
/****** Object:  Table [dbo].[attribute_DWH]    Script Date: 27.07.2017 12:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[attribute_DWH](
	[attributeID] [int] IDENTITY(1,1) NOT NULL,
	[entityID] [int] NOT NULL,
	[entityName] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[attributeName] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[columnID] [int] NOT NULL,
	[relAttributeID] [int] NULL,
	[typeName] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[is_nullable] [bit] NOT NULL,
	[defaultValue] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NULL,
	[sourceEntityName] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NULL,
	[sourceAttributeName] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NULL,
	[transformStr] [nvarchar](1024) COLLATE Cyrillic_General_CI_AS NULL,
	[is_ID] [bit] NOT NULL,
	[is_Code] [bit] NOT NULL,
	[is_Composite] [bit] NOT NULL,
	[is_Merge] [bit] NOT NULL,
	[is_Measure] [bit] NOT NULL,
	[is_FlagOperation] [bit] NOT NULL,
	[is_DateOperation] [bit] NOT NULL,
	[FlagDML] [bit] NOT NULL,
	[isUse] [bit] NOT NULL,
	[schemaName] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NULL,
	[isChangeDate] [bit] NULL,
	[isChangeDateTime] [bit] NULL,
	[collation] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NULL,
	[isChangeCode] [bit] NULL,
	[caption] [nvarchar](64) COLLATE Cyrillic_General_CI_AS NULL,
	[description] [nvarchar](256) COLLATE Cyrillic_General_CI_AS NULL,
PRIMARY KEY CLUSTERED 
(
	[attributeID] ASC
)WITH (PAD_INDEX = ON, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[attribute_DWH] ADD  DEFAULT ((0)) FOR [is_nullable]
GO
ALTER TABLE [dbo].[attribute_DWH] ADD  DEFAULT ((0)) FOR [is_ID]
GO
ALTER TABLE [dbo].[attribute_DWH] ADD  DEFAULT ((0)) FOR [is_Code]
GO
ALTER TABLE [dbo].[attribute_DWH] ADD  DEFAULT ((0)) FOR [is_Composite]
GO
ALTER TABLE [dbo].[attribute_DWH] ADD  DEFAULT ((0)) FOR [is_Merge]
GO
ALTER TABLE [dbo].[attribute_DWH] ADD  DEFAULT ((0)) FOR [is_Measure]
GO
ALTER TABLE [dbo].[attribute_DWH] ADD  DEFAULT ((0)) FOR [is_FlagOperation]
GO
ALTER TABLE [dbo].[attribute_DWH] ADD  DEFAULT ((0)) FOR [is_DateOperation]
GO
ALTER TABLE [dbo].[attribute_DWH] ADD  DEFAULT ((1)) FOR [FlagDML]
GO
ALTER TABLE [dbo].[attribute_DWH] ADD  DEFAULT ((1)) FOR [isUse]
GO
ALTER TABLE [dbo].[attribute_DWH]  WITH CHECK ADD FOREIGN KEY([entityID])
REFERENCES [dbo].[entity_DWH] ([entityID])
GO
