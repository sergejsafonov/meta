USE meta
GO

if object_id('dbo.operationLog') is not null
	DROP TABLE meta.dbo.operationLog
go
CREATE TABLE [dbo].[operationLog](										-- Лог операций обновления
	OperationLog int IDENTITY(1,1) NOT NULL PRIMARY KEY CLUSTERED,		-- уникальный идентификатор
	OperationType smallint NOT NULL DEFAULT(-1),						-- (-1) - ошибка, 1 - логирование
	OperationDate datetime NOT NULL DEFAULT (getdate()),				-- дата операции (процесса)
	OperationName nvarchar(256) NOT NULL,								-- имя операции (процесса)
	ProcedureName nvarchar(128) NULL,									-- имя процедуры
	OperationMessage nvarchar(4000) NULL,								-- содержание операции (процесса)
	LoginName nvarchar(128) NOT NULL,
	HostName nvarchar(128) NOT NULL,
	ProgramName nvarchar(128) NOT NULL,
	Duration money NULL,			-- [nchar](12)						-- продолжительность операции (процесса)
	WhatToDo nvarchar(4000) NULL,										-- описание инциндента (в случае ошибки)
	ErrorCode int NULL,													-- код ошибки в процедуре
	ErrorSource AS CONCAT(OperationName,'.',ProcedureName),				-- источник ошибки
	ProcedureLine int NULL												-- № строки процедуры, где произошла ошибка
									)
GO

select * from meta.dbo.OperationLog

/*
CREATE PROCEDURE test_error
AS

declare @typeLoad NVARCHAR(32) = 'incremental'

BEGIN TRY
	DECLARE @Duration MONEY

	SELECT @Duration = cast(total_elapsed_time as money)/1000
		FROM sys.dm_exec_requests 
		WHERE session_id = @@SPID

	IF (@Duration >= 900)	-- 15 min
		INSERT into meta.dbo.OperationLog (OperationName,ProcedureName,WhatToDo,LoginName,HostName,ProgramName,Duration)
			select @typeLoad AS OperationName,
				'DWH.pos.Purchase_upload' AS ProcedureName,
				'Долгое выполнение процедуры pos.Purchase_upload в бд DWH' AS WhatToDo, 
				s.loginame AS LoginName,
				s.hostname AS HostName,
				s.[program_name] AS ProgramName,
				cast(qs.total_elapsed_time as money)/1000 AS Duration
			from sys.sysprocesses s 
				INNER JOIN sys.dm_exec_requests qs ON s.spid = qs.session_id
			where s.spid = @@SPID

	SELECT 1/0
END TRY

BEGIN CATCH
	insert into meta.dbo.OperationLog (OperationName,ProcedureName,ErrorCode,ProcedureLine,OperationMessage,WhatToDo,LoginName,HostName,ProgramName,Duration)
				select @typeLoad AS OperationName,
					'DWH.pos.' + ERROR_PROCEDURE() AS ProcedureName,
					ERROR_NUMBER() AS ErrorCode,
					ERROR_LINE() AS ProcedureLine,
					ERROR_MESSAGE() AS OperationMessage,
					'Ошибка добавления нового чека розницы в pos.Purchase DbSourceID = 1 (posr) бд DWH' AS WhatToDo, 
					s.loginame AS LoginName,
					s.hostname AS HostName,
					s.program_name AS ProgramName,
					cast(qs.total_elapsed_time as money)/1000 AS Duration
				from sys.sysprocesses s 
					left join sys.dm_exec_requests qs ON s.spid = qs.session_id
				where s.spid = @@SPID
			
	SELECT   
        ERROR_NUMBER() AS ErrorNumber,  
        ERROR_SEVERITY() AS ErrorSeverity,  
        ERROR_STATE() AS ErrorState,  
        ERROR_PROCEDURE() AS ErrorProcedure,  
        ERROR_MESSAGE() AS ErrorMessage,  
        ERROR_LINE() AS ErrorLine;  
END CATCH

RETURN
GO

EXEC test_error

DROP PROCEDURE test_error
GO
*/

/*
begin catch
	insert into meta.dbo.operation_log (operation_type,operation_name,[procedure_name],error_code,procedure_line
				,operation_message,what_to_do)
			select 3,'Load_DWH_data','DWH.'+ERROR_PROCEDURE(),ERROR_NUMBER(),ERROR_LINE(),ERROR_MESSAGE()
				,'Ошибка установки флага обновления или добавления новой строки в таблице date БД DWH'
	raiserror('Error in procedure dbo.populate_date_cycle!',16,1)
	return
end CATCH

begin catch
	rollback transaction tranInsertSQL

	insert into meta.dbo.operation_log (operation_type,operation_name,procedure_name,error_code,procedure_line
			,operation_message,what_to_do)
		select 3,'Load_DWH_data','DWH.dbo.Insert_DWH!',ERROR_NUMBER(),ERROR_LINE(),ERROR_MESSAGE()
			,'Ошибка выполнения запроса загрузки новых данных в таблицу '+@entity_name+' БД DWH'

	raiserror('Error in procedure DWH.dbo.Insert_DWH!',16,1)
	return
end CATCH

insert into meta.dbo.operation_log (operation_type,operation_name,[procedure_name],operation_message)
	select 300,'Correct_DWH_data','DWH.dbo.Operate_expense_income_link!','Начало выполнения процедуры Operate_expense_income_link'

insert into meta.dbo.operation_log (operation_type,operation_name,[procedure_name],operation_message)
	select 310,'Correct_DWH_data','DWH.dbo.Operate_expense_income_link!','Конец выполнения процедуры Operate_expense_income_link'            
*/