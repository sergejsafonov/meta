USE [meta]
GO
/****** Object:  Table [dbo].[ClusterTable]    Script Date: 27.07.2017 12:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClusterTable](
	[UID] [int] IDENTITY(1,1) NOT NULL,
	[SAP_TableName] [nvarchar](50) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[STG_TableName] [nvarchar](10) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[STG_DBName] [nvarchar](50) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[Fields] [nvarchar](max) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[FieldsSQLTypes] [nvarchar](max) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[Delimiter1] [nvarchar](1) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[Delimiter2] [nvarchar](1) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[Delimiter3] [nvarchar](1) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[EstimatedSize] [bigint] NULL,
	[SegmentSize] [bigint] NULL,
	[SegmentKind] [nvarchar](1) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[LoadQueue] [nvarchar](max) COLLATE Cyrillic_General_CI_AS NULL,
	[Schedule] [nvarchar](max) COLLATE Cyrillic_General_CI_AS NULL,
	[FlagActive] [bit] NULL,
	[Aux] [nvarchar](max) COLLATE Cyrillic_General_CI_AS NULL,
	[Description] [nvarchar](max) COLLATE Cyrillic_General_CI_AS NULL,
PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = ON, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[ClusterTable] ADD  DEFAULT ('STG') FOR [STG_DBName]
GO
ALTER TABLE [dbo].[ClusterTable] ADD  DEFAULT ('') FOR [FieldsSQLTypes]
GO
ALTER TABLE [dbo].[ClusterTable] ADD  DEFAULT ('@') FOR [Delimiter1]
GO
ALTER TABLE [dbo].[ClusterTable] ADD  DEFAULT ('|') FOR [Delimiter2]
GO
ALTER TABLE [dbo].[ClusterTable] ADD  DEFAULT (';') FOR [Delimiter3]
GO
ALTER TABLE [dbo].[ClusterTable] ADD  DEFAULT ((0)) FOR [EstimatedSize]
GO
ALTER TABLE [dbo].[ClusterTable] ADD  DEFAULT ((0)) FOR [SegmentSize]
GO
ALTER TABLE [dbo].[ClusterTable] ADD  DEFAULT ('y') FOR [SegmentKind]
GO
ALTER TABLE [dbo].[ClusterTable] ADD  DEFAULT ('') FOR [LoadQueue]
GO
ALTER TABLE [dbo].[ClusterTable] ADD  DEFAULT ('') FOR [Schedule]
GO
ALTER TABLE [dbo].[ClusterTable] ADD  DEFAULT ((0)) FOR [FlagActive]
GO
EXEC sys.sp_addextendedproperty @name=N'Caption', @value=N'Размер сегмента для скачивания данных, в тыс строк' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ClusterTable', @level2type=N'COLUMN',@level2name=N'SegmentSize'
GO
