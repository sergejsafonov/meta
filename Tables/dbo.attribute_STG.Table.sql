USE [meta]
GO
/****** Object:  Table [dbo].[attribute_STG]    Script Date: 27.07.2017 12:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[attribute_STG](
	[attributeID] [int] IDENTITY(1,1) NOT NULL,
	[entityID] [int] NOT NULL,
	[entityName] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[attributeName] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[columnID] [int] NOT NULL,
	[typeName] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[is_nullable] [bit] NOT NULL,
	[defaultValue] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NULL,
	[is_ID] [bit] NOT NULL,
	[is_slidingDate] [bit] NOT NULL,
	[transformStr] [nvarchar](1024) COLLATE Cyrillic_General_CI_AS NULL,
	[refEntityName] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NULL,
	[refAttributeName] [nvarchar](1024) COLLATE Cyrillic_General_CI_AS NULL,
	[isUse] [bit] NOT NULL,
	[collation] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NULL,
	[schemaName] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NULL,
	[isChangeDate] [bit] NULL,
	[isChangeDateTime] [bit] NULL,
	[attributeDWHID] [int] NULL,
	[isPK] [bit] NOT NULL,
	[refSchemaName] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NULL,
	[is_slidingDate2] [bit] NOT NULL,
	[isAddPK] [bit] NOT NULL,
	[caption] [nvarchar](32) COLLATE Cyrillic_General_CI_AS NULL,
	[description] [nvarchar](256) COLLATE Cyrillic_General_CI_AS NULL,
PRIMARY KEY CLUSTERED 
(
	[attributeID] ASC
)WITH (PAD_INDEX = ON, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[attribute_STG] ADD  DEFAULT ((0)) FOR [is_nullable]
GO
ALTER TABLE [dbo].[attribute_STG] ADD  DEFAULT ((0)) FOR [is_ID]
GO
ALTER TABLE [dbo].[attribute_STG] ADD  DEFAULT ((0)) FOR [is_slidingDate]
GO
ALTER TABLE [dbo].[attribute_STG] ADD  DEFAULT ((1)) FOR [isUse]
GO
ALTER TABLE [dbo].[attribute_STG] ADD  DEFAULT ((0)) FOR [isPK]
GO
ALTER TABLE [dbo].[attribute_STG] ADD  DEFAULT ((0)) FOR [is_slidingDate2]
GO
ALTER TABLE [dbo].[attribute_STG] ADD  DEFAULT ((0)) FOR [isAddPK]
GO
ALTER TABLE [dbo].[attribute_STG]  WITH CHECK ADD FOREIGN KEY([attributeDWHID])
REFERENCES [dbo].[attribute_DWH] ([attributeID])
GO
ALTER TABLE [dbo].[attribute_STG]  WITH CHECK ADD FOREIGN KEY([entityID])
REFERENCES [dbo].[entity_STG] ([entityID])
GO
