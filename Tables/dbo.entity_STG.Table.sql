USE [meta]
GO
/****** Object:  Table [dbo].[entity_STG]    Script Date: 27.07.2017 12:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[entity_STG](
	[entityID] [int] IDENTITY(1,1) NOT NULL,
	[schemaName] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[entityName] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[alias] [nvarchar](8) COLLATE Cyrillic_General_CI_AS NULL,
	[object_id] [int] NULL,
	[addQueryStr] [nvarchar](1024) COLLATE Cyrillic_General_CI_AS NULL,
	[sortOrder] [smallint] NULL,
	[isClass] [bit] NOT NULL,
	[isMaterial] [bit] NOT NULL,
	[isCompany] [bit] NOT NULL,
	[isFact] [bit] NOT NULL,
	[isPOS] [bit] NOT NULL,
	[selectStr] [nvarchar](max) COLLATE Cyrillic_General_CI_AS NULL,
	[isUse] [bit] NOT NULL,
	[isIncremental] [bit] NOT NULL,
	[isNightFull] [bit] NOT NULL,
	[isWeekFull] [bit] NOT NULL,
	[isAddQueryStrIncremental] [bit] NOT NULL,
	[isDocumentType] [bit] NOT NULL,
	[clientField] [nvarchar](32) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[isCRM] [bit] NOT NULL,
	[isFullDelete] [bit] NOT NULL,
	[isPOSSet] [bit] NOT NULL,
	[isPOSLoy] [bit] NOT NULL,
	[isRstat] [bit] NOT NULL,
	[isTreasury] [bit] NOT NULL,
	[isUseSlidingDate] [bit] NOT NULL,
	[schemaAlias] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NULL,
	[isEveryHour] [bit] NOT NULL,
	[isFinance] [bit] NOT NULL,
	[caption] [nvarchar](32) COLLATE Cyrillic_General_CI_AS NULL,
	[description] [nvarchar](256) COLLATE Cyrillic_General_CI_AS NULL,
	[isAxPOS] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[entityID] ASC
)WITH (PAD_INDEX = ON, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ui_entity_stg_schemaName_entityName]    Script Date: 27.07.2017 12:19:20 ******/
CREATE UNIQUE NONCLUSTERED INDEX [ui_entity_stg_schemaName_entityName] ON [dbo].[entity_STG]
(
	[schemaName] ASC,
	[entityName] ASC
)WITH (PAD_INDEX = ON, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
ALTER TABLE [dbo].[entity_STG] ADD  DEFAULT ((0)) FOR [isClass]
GO
ALTER TABLE [dbo].[entity_STG] ADD  DEFAULT ((0)) FOR [isMaterial]
GO
ALTER TABLE [dbo].[entity_STG] ADD  DEFAULT ((0)) FOR [isCompany]
GO
ALTER TABLE [dbo].[entity_STG] ADD  DEFAULT ((0)) FOR [isFact]
GO
ALTER TABLE [dbo].[entity_STG] ADD  DEFAULT ((0)) FOR [isPOS]
GO
ALTER TABLE [dbo].[entity_STG] ADD  DEFAULT ((1)) FOR [isUse]
GO
ALTER TABLE [dbo].[entity_STG] ADD  DEFAULT ((1)) FOR [isIncremental]
GO
ALTER TABLE [dbo].[entity_STG] ADD  DEFAULT ((0)) FOR [isNightFull]
GO
ALTER TABLE [dbo].[entity_STG] ADD  DEFAULT ((0)) FOR [isWeekFull]
GO
ALTER TABLE [dbo].[entity_STG] ADD  DEFAULT ((0)) FOR [isAddQueryStrIncremental]
GO
ALTER TABLE [dbo].[entity_STG] ADD  DEFAULT ((0)) FOR [isDocumentType]
GO
ALTER TABLE [dbo].[entity_STG] ADD  DEFAULT ('MANDT') FOR [clientField]
GO
ALTER TABLE [dbo].[entity_STG] ADD  DEFAULT ((0)) FOR [isCRM]
GO
ALTER TABLE [dbo].[entity_STG] ADD  DEFAULT ((0)) FOR [isFullDelete]
GO
ALTER TABLE [dbo].[entity_STG] ADD  DEFAULT ((0)) FOR [isPOSSet]
GO
ALTER TABLE [dbo].[entity_STG] ADD  DEFAULT ((0)) FOR [isPOSLoy]
GO
ALTER TABLE [dbo].[entity_STG] ADD  DEFAULT ((0)) FOR [isRstat]
GO
ALTER TABLE [dbo].[entity_STG] ADD  DEFAULT ((0)) FOR [isTreasury]
GO
ALTER TABLE [dbo].[entity_STG] ADD  DEFAULT ((0)) FOR [isUseSlidingDate]
GO
ALTER TABLE [dbo].[entity_STG] ADD  DEFAULT ((0)) FOR [isEveryHour]
GO
ALTER TABLE [dbo].[entity_STG] ADD  DEFAULT ((0)) FOR [isFinance]
GO
ALTER TABLE [dbo].[entity_STG] ADD  DEFAULT ((0)) FOR [isAxPOS]
GO
/****** Object:  Trigger [dbo].[entity_STG_change]    Script Date: 27.07.2017 12:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		AG
-- Create date: 2017-05-30
-- Description:	Сохранение параметпрв обновления при изменении. Не работает при добалвении новой строки
-- =============================================
CREATE TRIGGER [dbo].[entity_STG_change]  
   ON  [dbo].[entity_STG]
   AFTER DELETE,UPDATE
AS 
BEGIN

Insert into [dbo].[entity_STG_save] (
	   [schemaName]
      ,[entityName]
      ,[alias]
      ,[object_id]
      ,[addQueryStr]
      ,[sortOrder]
      ,[isClass]
      ,[isMaterial]
      ,[isCompany]
      ,[isFact]
      ,[isPOS]
      ,[isUse]
      ,[isIncremental]
      ,[isNightFull]
      ,[isWeekFull]
      ,[isAddQueryStrIncremental]
      ,[isDocumentType]
      ,[clientField]
      ,[isCRM]
      ,[isFullDelete]
      ,[isPOSSet]
      ,[isPOSLoy]
      ,[isRstat]
      ,[isTreasury]
      ,[isUseSlidingDate]
      ,[schemaAlias]
      ,[isEveryHour]
      ,[isFinance]
      ,[caption]
      ,[description]
      ,[isAxPOS] 
	  ,usr
 	  ,Operation)
Select  
	   [schemaName]
      ,[entityName]
      ,[alias]
      ,[object_id]
      ,[addQueryStr]
      ,[sortOrder]
      ,[isClass]
      ,[isMaterial]
      ,[isCompany]
      ,[isFact]
      ,[isPOS]
      ,[isUse]
      ,[isIncremental]
      ,[isNightFull]
      ,[isWeekFull]
      ,[isAddQueryStrIncremental]
      ,[isDocumentType]
      ,[clientField]
      ,[isCRM]
      ,[isFullDelete]
      ,[isPOSSet]
      ,[isPOSLoy]
      ,[isRstat]
      ,[isTreasury]
      ,[isUseSlidingDate]
      ,[schemaAlias]
      ,[isEveryHour]
      ,[isFinance]
      ,[caption]
      ,[description]
      ,[isAxPOS] 
	  ,SYSTEM_USER
	  ,'Before'
From deleted

Insert into [dbo].[entity_STG_save] (
	   [schemaName]
      ,[entityName]
      ,[alias]
      ,[object_id]
      ,[addQueryStr]
      ,[sortOrder]
      ,[isClass]
      ,[isMaterial]
      ,[isCompany]
      ,[isFact]
      ,[isPOS]
      ,[isUse]
      ,[isIncremental]
      ,[isNightFull]
      ,[isWeekFull]
      ,[isAddQueryStrIncremental]
      ,[isDocumentType]
      ,[clientField]
      ,[isCRM]
      ,[isFullDelete]
      ,[isPOSSet]
      ,[isPOSLoy]
      ,[isRstat]
      ,[isTreasury]
      ,[isUseSlidingDate]
      ,[schemaAlias]
      ,[isEveryHour]
      ,[isFinance]
      ,[caption]
      ,[description]
      ,[isAxPOS] 
	  ,usr
	  ,OPeration)
Select  
	   [schemaName]
      ,[entityName]
      ,[alias]
      ,[object_id]
      ,[addQueryStr]
      ,[sortOrder]
      ,[isClass]
      ,[isMaterial]
      ,[isCompany]
      ,[isFact]
      ,[isPOS]
      ,[isUse]
      ,[isIncremental]
      ,[isNightFull]
      ,[isWeekFull]
      ,[isAddQueryStrIncremental]
      ,[isDocumentType]
      ,[clientField]
      ,[isCRM]
      ,[isFullDelete]
      ,[isPOSSet]
      ,[isPOSLoy]
      ,[isRstat]
      ,[isTreasury]
      ,[isUseSlidingDate]
      ,[schemaAlias]
      ,[isEveryHour]
      ,[isFinance]
      ,[caption]
      ,[description]
      ,[isAxPOS] 
	  ,SYSTEM_USER
	  ,'After'
From Inserted

END


GO
ALTER TABLE [dbo].[entity_STG] ENABLE TRIGGER [entity_STG_change]
GO
