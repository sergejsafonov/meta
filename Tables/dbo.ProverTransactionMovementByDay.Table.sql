USE [meta]
GO
/****** Object:  Table [dbo].[ProverTransactionMovementByDay]    Script Date: 27.07.2017 12:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProverTransactionMovementByDay](
	[MovementDate] [int] NOT NULL,
	[DestCnt] [bigint] NOT NULL,
	[SrcCnt] [bigint] NOT NULL,
	[ExecuteDate] [smalldatetime] NULL
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[ProverTransactionMovementByDay] ADD  DEFAULT ((0)) FOR [DestCnt]
GO
ALTER TABLE [dbo].[ProverTransactionMovementByDay] ADD  DEFAULT ((0)) FOR [SrcCnt]
GO
ALTER TABLE [dbo].[ProverTransactionMovementByDay] ADD  DEFAULT (getdate()) FOR [ExecuteDate]
GO
