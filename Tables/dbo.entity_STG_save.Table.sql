USE [meta]
GO
/****** Object:  Table [dbo].[entity_STG_save]    Script Date: 27.07.2017 12:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[entity_STG_save](
	[entityID] [int] IDENTITY(1,1) NOT NULL,
	[SaveDate] [datetime] NOT NULL,
	[schemaName] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[entityName] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[alias] [nvarchar](8) COLLATE Cyrillic_General_CI_AS NULL,
	[object_id] [int] NULL,
	[addQueryStr] [nvarchar](1024) COLLATE Cyrillic_General_CI_AS NULL,
	[sortOrder] [smallint] NULL,
	[isClass] [bit] NOT NULL,
	[isMaterial] [bit] NOT NULL,
	[isCompany] [bit] NOT NULL,
	[isFact] [bit] NOT NULL,
	[isPOS] [bit] NOT NULL,
	[isUse] [bit] NOT NULL,
	[isIncremental] [bit] NOT NULL,
	[isNightFull] [bit] NOT NULL,
	[isWeekFull] [bit] NOT NULL,
	[isAddQueryStrIncremental] [bit] NOT NULL,
	[isDocumentType] [bit] NOT NULL,
	[clientField] [nvarchar](32) COLLATE Cyrillic_General_CI_AS NOT NULL,
	[isCRM] [bit] NOT NULL,
	[isFullDelete] [bit] NOT NULL,
	[isPOSSet] [bit] NOT NULL,
	[isPOSLoy] [bit] NOT NULL,
	[isRstat] [bit] NOT NULL,
	[isTreasury] [bit] NOT NULL,
	[isUseSlidingDate] [bit] NOT NULL,
	[schemaAlias] [nvarchar](128) COLLATE Cyrillic_General_CI_AS NULL,
	[isEveryHour] [bit] NOT NULL,
	[isFinance] [bit] NOT NULL,
	[caption] [nvarchar](32) COLLATE Cyrillic_General_CI_AS NULL,
	[description] [nvarchar](256) COLLATE Cyrillic_General_CI_AS NULL,
	[isAxPOS] [bit] NOT NULL,
	[usr] [nvarchar](100) COLLATE Cyrillic_General_CI_AS NULL,
	[Operation] [nvarchar](100) COLLATE Cyrillic_General_CI_AS NULL,
PRIMARY KEY CLUSTERED 
(
	[entityID] ASC,
	[SaveDate] DESC
)WITH (PAD_INDEX = ON, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[entity_STG_save] ADD  DEFAULT (getdate()) FOR [SaveDate]
GO
